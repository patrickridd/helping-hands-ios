# Helping Hands

The "Helping Hands" project is an iOS App that aims to help families with children that have cancer. The app will connect families with those that want to help provide services for children with cancer.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

You can run all tests in project by -

* Tap "Command+U" 
* Go to "Product" menu and tap "Test"

## Built With

* [CocoaPods](https://cocoapods.org) - Dependency Management
* [AWS Amplify](https://aws.amazon.com/amplify/) - Helps manage AWS

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

Software Technology Group Dev Center

## License

© STG Consulting 2019. All rights reserved.

## Acknowledgments

TODO