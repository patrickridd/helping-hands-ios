//
//  SignInPresenterTests.swift
//  helping-hands-iosTests
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 10/30/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import XCTest
import AWSMobileClient
@testable import helping_hands_ios


class MockSignInView: SignInViewDelegate {
    
    var presenter: SignInPresenterLogic!
    
    var presentsAWSSign: Bool = false
    var navigatesToMainTabBarView: Bool = false
    
    func presentAWSSignIn(with signInOptions: SignInUIOptions) {
        presentsAWSSign = true
    }
    
    func navigate(to mainTabBarController: UIViewController) {
        navigatesToMainTabBarView = true
    }
    
    func checkIfSignedIn() {}
    
}

class SignInPresenterTests: XCTestCase {
    
    
    var presenter: SignInPresenterLogic!
    var view: MockSignInView!
    var user: User!
    
    override func setUp() {
        
        user = User()
        view = MockSignInView()
        presenter = SignInPresenter(view: view,
                                    model: user!)
        view.presenter = presenter
    }
    
    override func tearDown() {
        user = nil
        view = nil
        presenter = nil
    }
    
    func testCheckIfSignedInPresentsAWSSignIn() {
       
    }
    
    func testCheckIfSignedInNavigatesToHomeScreen() {
 
    }
    
}
