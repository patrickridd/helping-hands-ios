//
//  SignInViewControllerTests.swift
//  helping-hands-iosTests
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 10/30/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import XCTest
@testable import helping_hands_ios

class MockSignInPresenter: SignInPresenterLogic {
    
    var checkIfSignedInCalled: Bool = false
    var presentedAWSSignIn: Bool = false

    weak var view: SignInViewDelegate?
    var model: User?
    
    required init(view: SignInViewDelegate, model: Any) {
        self.view = view
        self.model = model as? User
    }
    
    func checkIfSignedIn() {
        checkIfSignedInCalled = true
    }
    
    func presentAWSSignIn() {
        presentedAWSSignIn = true
    }
    
    func navigateToContainerView() {
    
    }
    
}

class SignInViewControllerTests: XCTestCase {

    var presenter: MockSignInPresenter!
    var view: SignInViewDelegate!
    var user: User!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        user = User()
        view = SignInViewController()
        presenter = MockSignInPresenter(view: view, model: user!)
        view.presenter = presenter
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        user = nil
        view = nil
        presenter = nil
    }

    func testCheckIfSignedInCallsCheckIfSigned() {
        view.checkIfSignedIn()
        XCTAssertTrue(presenter.checkIfSignedInCalled)
    }
    
    
}
