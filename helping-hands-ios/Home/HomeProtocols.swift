//
//  HomeProtocols.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 10/30/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

protocol HomePresenterLogic: class {
    var view: HomeViewDelegate? { get set }
    init(view: HomeViewDelegate)
    
    func getLocation()
    func centerButtonTapped()
    func requestButtonTapped()
    func segment(selected: Int)
    func getRequests()
    func tappedCallOut(for request: Request)
}

protocol HomeViewDelegate: class {
    var presenter: HomePresenterLogic! { get set }
    func set(location: CLLocation)
    func centerButtonTapped()
    func requestButtonTapped()
    func presentRequestListView()
    func present(view: UIViewController)
    func centerMap()
    func set(radius: Double)
    func set(annotations: [RequestAnnotation])
    func presentDetailView(for request: Request)
}


