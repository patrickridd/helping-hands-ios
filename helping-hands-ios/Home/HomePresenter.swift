//
//  HomePresenter.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 10/30/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import AWSAuthCore
import AWSMobileClient
import MapKit

class HomePresenter: HomePresenterLogic {
   
    let locationManager = LocationServiceManager()
    var view: HomeViewDelegate?
    var fetchedRequests: [Request] = []
    
    required init(view: HomeViewDelegate) {
        self.view = view
    }
    
    func milesToMeters(miles: Int) -> Double {
        return Double(miles) * 1609.34
    }
    
    func getLocation() {
        locationManager.requestWhenInUseAuthorization()
        
        if locationManager.locationServicesEnabled() {
            locationManager.getLocation(for: self)
        }
    }
    
    func centerButtonTapped() {
        view?.centerMap()
    }
    
    func segment(selected: Int) {
        switch selected {
        case 0:
            view?.set(radius: milesToMeters(miles: 1))
        case 1:
            view?.set(radius: milesToMeters(miles: 5))
        case 2:
            view?.set(radius: milesToMeters(miles: 10))
        default:
            view?.set(radius: milesToMeters(miles: 15))
        }
    }
    
    func requestButtonTapped() {
        view?.presentRequestListView()
    }
    
    func getRequests() {
        self.fetchedRequests =
            [Request(id: "", requester: User(), title: "Clean Car", description: "I have vacuum and other necessary cleaning supplies", latitude: 40.761009, longitude:  -111.885082, address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil), duration: "3 hours", requestDate: Date().add(days: 2)),
             Request(id: "", requester: User(), title: "Laundry", description: "I have detergent and the clothes separated", latitude: 40.759263, longitude: -111.881654, address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil), duration: "2 hours", requestDate: Date().add(days: 2)),
             Request(id: "", requester: User(), title: "Drive to airport", description: "Thank you.", latitude: 40.756004, longitude: -111.881944, address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil), duration: "45 minutes", requestDate: Date()),
             Request(id: "", requester: User(), title: "Help clean up yard",description: "I need the lawn mowed and some leaves raked", latitude: 40.754744, longitude: -111.884916, address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil), duration: "3 hours", requestDate: Date()),
             Request(id: "", requester: User(), title: "Need help with dinner", description: "I have a child who is allergic to legumes so I ask that we avoid anything with those.", latitude: 40.754077, longitude: 111.882845, address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil), duration: "3 hours", requestDate: Date()),
             Request(id: "", requester: User(), title: "Vacuum",description: "I don't have a vacuum so I would need someone to supply one", latitude: 40.758445, longitude: -111.885034, address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil), duration: "3 hours", requestDate: Date()),
             Request(id: "", requester: User(), title: "Help clean kitchen", description: "I have all of the cleaning supplies", latitude: 40.756950, longitude: -111.886032, address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil), duration: "3 hours", requestDate: Date().add(days: 9)),
             Request(id: "", requester: User(), title: "Shovel Snow", description: "I have two shovels and also have salt", latitude: 40.757535, longitude: -111.892566, address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil), duration: "1 hours", requestDate: Date().add(days: 3)),
             Request(id: "", requester: User(), title: "Mow Lawn", description: "Description", latitude: 40.760700, longitude: -111.872461, address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil), duration: "3 hours", requestDate: Date().add(days: 5))]

        
        var annotations: [RequestAnnotation] = []
        
        for request in fetchedRequests {
            let annotation = RequestAnnotation(coordinate: request.location.coordinate,
                                               request: request)
            
            annotation.title = request.title
            annotation.subtitle = "Due date: \(request.requestDate.formatDateForDisplay())"
            annotations.append(annotation)
            
        }
        view?.set(annotations: annotations)
    }
    
    func tappedCallOut(for request: Request) {
        view?.presentDetailView(for: request)
    }
    
}

extension HomePresenter: LocationListener {
    
    func didReceive(location: CLLocation, address: Address) {
        view?.set(location: location)
    }
    
}
