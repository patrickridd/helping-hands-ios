//
//  HomeViewController.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 10/30/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit
import MapKit
import AWSMobileClient

class HomeViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var centerButton: UIButton!
    @IBOutlet weak var requestButton: UIButton!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    
    var presenter: HomePresenterLogic!
    var regionRadius: CLLocationDistance = 1609.34 // Set radius to 1 mile by default
    var currentLocation: CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter = HomePresenter(view: self)
        mapView.delegate = self
        
        requestButton.addTarget(self, action: #selector(requestButtonTapped), for: .touchUpInside)
        centerButton.addTarget(self,
                               action: #selector(centerButtonTapped),
                               for: .touchUpInside)
        segmentedControl.addTarget(self, action: #selector(radiusChanged), for: .valueChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter.getLocation()
    }
    
}

extension HomeViewController: HomeViewDelegate {
    
    func set(location: CLLocation) {
        currentLocation = location
        DispatchQueue.main.async {
            self.centerMap()
            self.presenter.getRequests()
        }
    }
    
    @objc func centerButtonTapped() {
        presenter.centerButtonTapped()
    }
    
    func centerMap() {
        if let location = currentLocation {
            let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                      latitudinalMeters: self.regionRadius,
                                                      longitudinalMeters: self.regionRadius)
            self.mapView.showsUserLocation = true
            self.mapView.setRegion(coordinateRegion, animated: true)
        }
    }
    
    @objc func radiusChanged() {
        presenter.segment(selected: segmentedControl.selectedSegmentIndex)
    }
    
    func set(radius: Double) {
        regionRadius = radius
        centerMap()
    }
    
    @objc func requestButtonTapped() {
        presenter.requestButtonTapped()
    }
    
    func present(view: UIViewController) {
        present(view, animated: true, completion: nil)
    }
    
    func set(annotations: [RequestAnnotation]) {
        mapView.addAnnotations(annotations)
    }
    
    func presentRequestListView() {
        let storyboard = UIStoryboard(name: "RequestList", bundle: nil)
        if let requestListView = storyboard.instantiateViewController(identifier: "RequestListView") as? RequestListViewController {
            let navController = UINavigationController(rootViewController: requestListView)
            requestListView.title = "Requests"
            present(navController, animated: true, completion: nil)
        }
    }
    
    func presentDetailView(for request: Request) {
        let requestDetailView = RequestDetailViewController(nibName: "RequestDetail", bundle: nil)
        requestDetailView.request = request
        present(requestDetailView, animated: true, completion: nil)
    }
    
}

extension HomeViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let identifier = "mapMarker"
        var view: MKMarkerAnnotationView
        
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            let button = UIButton(type: .detailDisclosure)
            
            view.rightCalloutAccessoryView = button
        }
        return view
    }
    
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if let requestAnnotation = view.annotation as? RequestAnnotation {
            let request = requestAnnotation.request
            presenter.tappedCallOut(for: request)
        }
    }
    
}
