//
//  RegisterChoiceProtocols.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/5/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation


protocol RegisterChoicePresenterLogic {
    // Weak Reference (avoid retain cycle)
    var view: RegisterChoiceViewDelegate? { get set }
    init(view: RegisterChoiceViewDelegate)

    func closeView()
    func createHelpedAccountButtonTapped()
    func createHelperAccountButtonTapped()
}


protocol RegisterChoiceViewDelegate: class {
    // Strong Reference
    var presenter: RegisterChoicePresenterLogic! { get set }
    
    func closeButtonTapped()
    func closeView()
    func createHelpedAccountButtonTapped()
    func navigateToContactView()
    func createHelperAccountButtonTapped()
}
