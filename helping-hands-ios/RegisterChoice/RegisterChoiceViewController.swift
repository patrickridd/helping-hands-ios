//
//  RegisterChoiceViewController.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/4/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

class RegisterChoiceViewController: UIViewController, RegisterChoiceViewDelegate {
    
    @IBOutlet weak var createHelpedAccountButton: UIButton!
    @IBOutlet weak var createHelperAccountButton: UIButton!
    
    var presenter: RegisterChoicePresenterLogic!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.presenter = RegisterChoicePresenter(view: self)
            
        // Add "Done" button
        let doneButton = UIBarButtonItem(barButtonSystemItem: .cancel,
                                         target: self,
                                         action: #selector(closeButtonTapped))
        doneButton.tintColor = .black
       
        createHelpedAccountButton.addTarget(self,
                                            action: #selector(createHelpedAccountButtonTapped),
                                            for: .touchUpInside)
        createHelperAccountButton.addTarget(self,
                                            action: #selector(createHelperAccountButtonTapped),
                                            for: .touchUpInside)
        
        navigationItem.rightBarButtonItem = doneButton
        
        self.navigationItem.hidesBackButton = true
        doneButton.action = #selector(closeButtonTapped)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = false
    }
    
}


extension RegisterChoiceViewController {
    
    @objc func closeButtonTapped() {
        presenter.closeView()
    }
    
    func closeView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func createHelpedAccountButtonTapped() {
        presenter.createHelpedAccountButtonTapped()
    }
    
    @objc func createHelperAccountButtonTapped() {
        presenter.createHelperAccountButtonTapped()
    }
    
    func navigateToContactView() {
        let contactView = ContactViewController(nibName: "Contact", bundle: nil)
        navigationController?.navigationBar.isHidden = true
        navigationController?.pushViewController(contactView, animated: true)
    }
}
