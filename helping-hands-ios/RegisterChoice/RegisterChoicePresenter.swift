//
//  RegisterChoicePresenter.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/4/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation

class RegisterChoicePresenter: RegisterChoicePresenterLogic {
   
   
    var view: RegisterChoiceViewDelegate?
    
    required init(view: RegisterChoiceViewDelegate) {
        self.view = view
    }
    
    func closeView() {
        view?.closeView()
    }
    
    func createHelpedAccountButtonTapped() {
        // Save helped selection
        
        view?.navigateToContactView()
    }
       
    func createHelperAccountButtonTapped() {
        // Save helper selection
        
        view?.navigateToContactView()
    }
    
}
