//
//  ContainerProtocols.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 12/4/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

protocol ContainerViewDelegate {
    var centerView: UIViewController! { get set }
    var presenter: ContainerPresenterDelegate! { get set }
    
    func setupNavigation(with tabBarController: MainTabBarViewController)
    func showMenu(shouldExpand: Bool)
    func presentAccountView()
    func logOut()
}

protocol ContainerPresenterDelegate {
    var view: ContainerViewDelegate? { get set }
    init(view: ContainerViewDelegate)
    
    func setupNavigationHeirarchy()
    func handleMenuToggle(isExpanding: Bool)
    func didSelect(menuOption: MenuOption)
}
