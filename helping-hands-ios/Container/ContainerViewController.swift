//
//  ContainerViewController.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 12/4/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {
    
    var centerView: UIViewController!
    var menuView: MenuViewController!
    var presenter: ContainerPresenterDelegate!
    var isExpanded: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter = ContainerPresenter(view: self)
        self.navigationController?.navigationBar.isHidden = true
        
        let leftSwipeGesture = UISwipeGestureRecognizer(target: self,
                                                        action: #selector(swipedLeftGestureRecognized))
        leftSwipeGesture.direction = .left
        view.addGestureRecognizer(leftSwipeGesture)
        
        presenter.setupNavigationHeirarchy()
    }
    
    
}

extension ContainerViewController: ContainerViewDelegate {
    
    func setupNavigation(with tabBarController: MainTabBarViewController) {
        tabBarController.menuDrawerDelegate = self
        centerView = UINavigationController(rootViewController: tabBarController)
        view.addSubview(centerView.view)
        addChild(centerView)
        centerView.didMove(toParent: self)
    }
    
    func configureMenuView() {
        if menuView == nil {
            menuView = MenuViewController()
            menuView.delegate = self
            view.insertSubview(menuView.view, at: 0)
            addChild(menuView)
            menuView.didMove(toParent: self)
        }
    }
    
    func showMenu(shouldExpand: Bool) {
       
        if shouldExpand {
            // show menu
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
                self.centerView.view.frame.origin.x = self.centerView.view.frame.width - 80
            }, completion: nil)
        } else {
            // hide menu
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
                self.centerView.view.frame.origin.x = 0
            }, completion: nil)
        }
    }
    
    func presentAccountView() {
          let storyboard = UIStoryboard(name: "Account", bundle: nil)
          if let accountView = storyboard.instantiateViewController(identifier: "AccountView") as? AccountViewController {
              let navController = UINavigationController(rootViewController: accountView)
              present(navController, animated: true, completion: nil)
          }
    }
    
    func logOut() {
        showMenu(shouldExpand: false)
        navigationController?.popViewController(animated: true)
    }
    
    @objc func swipedLeftGestureRecognized() {
        isExpanded = !isExpanded
        presenter.handleMenuToggle(isExpanding: false)
    }
}

extension ContainerViewController: MenuDrawerDelegate {
    
    func handleMenuToggle(for menuOption: MenuOption?) {
        if !isExpanded {
            configureMenuView()
        }
        
        isExpanded = !isExpanded
        presenter.handleMenuToggle(isExpanding: isExpanded)
        if let menuOption = menuOption {
            presenter.didSelect(menuOption: menuOption)
        }
    }
}
