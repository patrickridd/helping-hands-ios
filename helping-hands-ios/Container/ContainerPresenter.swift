//
//  ContainerPresenter.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 12/4/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation
import AWSMobileClient

class ContainerPresenter: ContainerPresenterDelegate {
   
    var view: ContainerViewDelegate?
    
    required init(view: ContainerViewDelegate) {
        self.view = view
    }
    
    func setupNavigationHeirarchy() {
        let mainTabBarController = MainTabBarViewController()
        view?.setupNavigation(with: mainTabBarController)
    }
    
    func handleMenuToggle(isExpanding: Bool) {
        view?.showMenu(shouldExpand: isExpanding)
    }
    
    func didSelect(menuOption: MenuOption) {
        switch menuOption {
        case .profile:
            view?.presentAccountView()
        case .logOut:
            AWSMobileClient.default().signOut { (_) in
                self.view?.logOut()
            }
        }
    }
}
