//
//  AppDelegate.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 10/24/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit
import AWSMobileClient
import AWSAppSync

class MyAuthProvider: AWSCognitoUserPoolsAuthProviderAsync {
    
    func getLatestAuthToken(_ callback: @escaping (String?, Error?) -> Void) {
        AWSMobileClient.default().getTokens { (tokens, error) in
            callback(tokens?.idToken?.tokenString, error)
        }
        
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var appSyncClient: AWSAppSyncClient?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Override point for customization after application launch.
       
        AWSMobileClient.default().initialize { (userState, error) in
                 print(userState)
                 print(error)
             }
        do {
              // You can choose the directory in which AppSync stores its persistent cache databases
              let cacheConfiguration = try AWSAppSyncCacheConfiguration()

              // AppSync configuration & client initialization
              let appSyncServiceConfig = try AWSAppSyncServiceConfig()
                  let appSyncConfig = try AWSAppSyncClientConfiguration(appSyncServiceConfig: appSyncServiceConfig,
                                                                        userPoolsAuthProvider: MyAuthProvider(),
                                                                        cacheConfiguration: cacheConfiguration)
                 
                  appSyncClient = try AWSAppSyncClient(appSyncConfig: appSyncConfig)
                  // Set id as the cache key for objects. See architecture section for details
                  appSyncClient?.apolloClient?.cacheKeyForObject = { $0["id"] }
             
        } catch {
                  print("Error initializing appsync client. \(error)")
              }
        
        let awsStarted = AWSMobileClient.default().interceptApplication(application,
                                                                        didFinishLaunchingWithOptions: launchOptions)
        return awsStarted
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

