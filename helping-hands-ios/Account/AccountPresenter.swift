//
//  AccountPresenter.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/8/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import AWSMobileClient

class AccountPresenter: AccountPresenterLogic {
    var view: (AccountViewDelegate & UIViewController)?
    let photoManager = PhotoServiceManager()

    
    required init(view: (AccountViewDelegate & UIViewController)?) {
        self.view = view
    }
    
    func signOutButtonTapped() {
        AWSMobileClient.default().signOut { [weak self] (error) in
            if let error = error {
                print(error.localizedDescription)
            }
            
            self?.view?.navigateToSignIn()
        }
    }
    
    func updateButtonTapped() {
        view?.presentImageAlertController()
    }
    
    func showCamera() {
        photoManager.showCamera(on: view!, using: self)
    }
    
    func showPhotoLibrary() {
        photoManager.showPhotoLibrary(on: view!, using: self)
    }
    
}

extension AccountPresenter: PhotoReceiver {
    
    func didReceive(image: UIImage) {
        view?.setUser(image: image)
    }
    
}
