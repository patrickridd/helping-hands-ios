//
//  AccountViewController.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/8/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var signOutButton: UIButton!
    
    var presenter: AccountPresenterLogic!
    var userImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Adjusts UI for grouped tableView style
        edgesForExtendedLayout = .all
        let tableHeaderView = UIView(frame: CGRect(x: 0,
                                                   y: 0,
                                                   width: tableView.bounds.width,
                                                   height: 0.01))
        tableView.tableHeaderView = tableHeaderView
        
        // Do any additional setup after loading the view.
        presenter = AccountPresenter(view: self)
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
}

extension AccountViewController: AccountViewDelegate {

    @objc func updateButtonTapped() {
        presenter.updateButtonTapped()
    }
    
    @objc func signOutButtonTapped() {
        presenter.signOutButtonTapped()
    }
    
    func navigateToSignIn() {
        DispatchQueue.main.async {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func presentImageAlertController() {
        let alertController = UIAlertController(title: "Profile Picture",
                                                message: nil,
                                                preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
            alertController.dismiss(animated: true, completion: nil)
        }
        let photoAction = UIAlertAction(title: "Photo Library", style: .default) { [weak self] (_) in
            self?.presenter.showPhotoLibrary()
        }
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { [weak self] (_) in
            self?.presenter.showCamera()
        }
        alertController.addAction(cameraAction)
        alertController.addAction(photoAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func setUser(image: UIImage) {
        userImage = image
        let indexPath = IndexPath(row: 0, section: 0)
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
}

extension AccountViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let accountImageCell = tableView.dequeueReusableCell(withIdentifier: "accountImageCell",
                                                                 for: indexPath) as? AccountImageTableViewCell
           
            accountImageCell?.updateButton.addTarget(self,
                                                     action: #selector(updateButtonTapped),
                                                     for: .touchUpInside)
            accountImageCell?.set(image: userImage ?? UIImage(systemName: "person.circle")!)
            
            return accountImageCell ?? UITableViewCell()
            
        case 1:
            let accountSignOutCell = tableView.dequeueReusableCell(withIdentifier: "signOutCell",
                                                                   for: indexPath) as? AccountSignOutTableViewCell
            accountSignOutCell?.signOutButton.addTarget(self,
                                                        action: #selector(signOutButtonTapped),
                                                        for: .touchUpInside)
            return accountSignOutCell ?? UITableViewCell()
        
        default:
            return UITableViewCell()
        }
    }
    
}

extension AccountViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 230
    }
    
}
