//
//  AccountProtocols.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/8/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

protocol AccountPresenterLogic {
    var view: (AccountViewDelegate & UIViewController)? { get set }
    init(view: (AccountViewDelegate & UIViewController)?)
    
    func updateButtonTapped()
    func signOutButtonTapped()
    func showCamera() 
    func showPhotoLibrary()
    
}

protocol AccountViewDelegate {
    var presenter: AccountPresenterLogic! { get set }
    
    func updateButtonTapped()
    func signOutButtonTapped()
    func navigateToSignIn()
    func presentImageAlertController()
    func setUser(image: UIImage)

}
