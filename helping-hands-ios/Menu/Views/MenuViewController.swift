//
//  MenuViewController.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 12/4/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

private let reuseIdentifier = "MenuCell"

class MenuViewController: UIViewController {

    var presenter: MenuPresenterDelegate!
    
    var tableView: UITableView!
    
    weak var delegate: MenuDrawerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter = MenuPresenter(view: self)
        view.backgroundColor = .darkGray
        
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .darkGray

        tableView.rowHeight = 80

        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        
        tableView.register(MenuTableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
    }
    

}

extension MenuViewController: MenuViewDelegate {
    
}

extension MenuViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MenuOption.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let menuCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? MenuTableViewCell
        
        let menuOption = MenuOption(rawValue: indexPath.row)
        menuCell?.update(with: menuOption)
        
        return menuCell ?? UITableViewCell()
    }
}

extension MenuViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuOption = MenuOption(rawValue: indexPath.row)
        delegate?.handleMenuToggle(for: menuOption)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
