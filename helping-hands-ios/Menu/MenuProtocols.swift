//
//  MenuProtocols.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 12/4/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation

protocol MenuPresenterDelegate {
    var view: MenuViewDelegate? { get }
    init(view: MenuViewDelegate)
}

protocol MenuViewDelegate {
    var presenter: MenuPresenterDelegate! { get set }
    
}

protocol MenuDrawerDelegate: class {
    func handleMenuToggle(for menuOption: MenuOption?)
}
