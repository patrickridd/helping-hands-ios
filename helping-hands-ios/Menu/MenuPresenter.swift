//
//  MenuPresenter.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 12/4/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation

class MenuPresenter: MenuPresenterDelegate {
    
    var view: MenuViewDelegate?
    
    required init(view: MenuViewDelegate) {
        self.view = view
    }
    
}
