//
//  HelperTableViewCell.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/18/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

class HelperTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var acceptButton: ActionButton!
    @IBOutlet weak var blockButton: ActionButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(with helper: User) {
        nameLabel.text = helper.fullName
        if let rating = helper.helperRating {
            ratingLabel.text = "\(rating)/5"
        } else {
            ratingLabel.text = "* / 5"
        }
    }
    
    func set(indexPath: IndexPath) {
        acceptButton.indexPath = indexPath
        blockButton.indexPath = indexPath
    }
}

