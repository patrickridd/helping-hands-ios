//
//  KeyboardToolbar.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/15/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

class KeyboardToolbar: UIToolbar {
    
    var textField: UITextField?
    var textView: UITextView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
       
        barStyle = .default
        
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        items = [space,
                 space,
                 UIBarButtonItem(title: "Done",
                                 style: .done,
                                 target: self,
                                 action: #selector(resignTextFields))]
        sizeToFit()
    }
    
    convenience init(frame: CGRect = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50),
                     textField: UITextField? = nil,
                     textView: UITextView? = nil) {
        self.init(frame: frame)
        self.textField = textField
        self.textView = textView
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func resignTextFields() {
        textField?.resignFirstResponder()
        textView?.resignFirstResponder()
    }
}
