//
//  DayButton.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/15/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

class DayButton: UIButton {

    var day: Day?
    
    func updateWith(day: Day?, buttonTag: Int, isSelected: Bool? = nil) {
        setTitle(day?.dayOfTheWeek.description, for: .normal)
        self.day = day
        tag = buttonTag
        if let isSelected = isSelected {
            button(isSelected: isSelected)
        }
    }
    
    func button(isSelected: Bool) {
        if isSelected {
            layer.cornerRadius = 20
        } else {
            layer.cornerRadius = 0
        }
    }
}
