//
//  AppSyncService.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 2/19/20.
//  Copyright © 2020 patrickridd. All rights reserved.
//

import Foundation
import AWSAppSync

protocol AppSyncServiceDelegate {
    
    func createUser(with id: String, email: String, phone: String, completion: @escaping (_ error: Error?) -> Void)
    func create(request: Request, completion: (_ error: Error?) -> Void)
    func update(user: User, completion: @escaping (_ error: Error?) -> Void)
    func update(request: Request, completion: (_ error: Error) -> Void)
    
}


struct AppSyncService: AppSyncServiceDelegate {
   
    let appSyncClient: AWSAppSyncClient? = (UIApplication.shared.delegate as! AppDelegate).appSyncClient

    func createUser(with id: String, email: String, phone: String, completion: @escaping (_ error: Error?) -> Void) {
        let createUserInput = CreateUserInput(id: id, email: email, phoneNumber: phone)
        let createUserMutation = CreateUserMutation(input: createUserInput)
        
        appSyncClient?.perform(mutation: createUserMutation, queue: .main, optimisticUpdate: nil, conflictResolutionBlock: nil, resultHandler: { (result, error) in
            completion(error)
        })
    }
    
    func create(request: Request, completion: (Error?) -> Void) {
        
    }
    
    func update(user: User, completion: @escaping (Error?) -> Void) {
        let userUpdateInput = UpdateUserInput(id: user.id,
                                              email: user.email,
                                              phoneNumber: user.phoneNumber,
                                              firstName: user.firstName,
                                              lastName: user.lastName,
                                              preferredContact: user.preferredContact,
                                              helperRatings: user.helperRatings,
                                              imageUrl: user.imageUrl)
       
        let updateMutation = UpdateUserMutation(input: userUpdateInput)
        
        appSyncClient?.perform(mutation: updateMutation, queue: .main, optimisticUpdate: { (transaction) in
            do {
                try transaction?.update(query: GetUserQuery(id: user.id)) { (data: inout GetUserQuery.Data) in
                    data.getUser?.firstName = user.firstName
                    data.getUser?.lastName = user.lastName
                    data.getUser?.imageUrl = user.imageUrl
                    data.getUser?.preferredContact = user.preferredContact
                    data.getUser?.helperRatings = user.helperRatings
                    data.getUser?.phoneNumber = user.phoneNumber
                    data.getUser?.email = user.email
                }
                
            } catch let error {
                completion(error)
            }
        })
    }
    
    func update(request: Request, completion: (Error) -> Void) {
        
    }
    
}
