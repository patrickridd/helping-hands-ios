//
//  PhotoServiceManager.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/8/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

protocol PhotoServiceManagerLogic {
    var delegate: PhotoReceiver? { get set }
    func showCamera(on view: UIViewController, using receiver: PhotoReceiver)
    func showPhotoLibrary(on view: UIViewController, using receiver: PhotoReceiver)
}

protocol PhotoReceiver: class {
    func didReceive(image: UIImage)
}

class PhotoServiceManager: UIViewController, PhotoServiceManagerLogic {
  
    var presentingView: UIViewController?
    weak var delegate: PhotoReceiver?
    
    func showCamera(on view: UIViewController, using receiver: PhotoReceiver) {
        self.presentingView = view
        self.delegate = receiver
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .camera
            presentingView?.present(myPickerController, animated: true, completion: nil)
        }
    }
      
    func showPhotoLibrary(on view: UIViewController, using receiver: PhotoReceiver) {
        self.presentingView = view
        self.delegate = receiver
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            presentingView?.present(myPickerController, animated: true, completion: nil)
        }
    }

}

extension PhotoServiceManager: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
  
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        presentingView?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            // Call Protocol method to send image
            delegate?.didReceive(image: image)
        } else {
            print("Something went wrong")
        }
        
        presentingView?.dismiss(animated: true, completion: nil)
    }
    
}
