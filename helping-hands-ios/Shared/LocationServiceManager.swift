//
//  LocationServiceManager.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/7/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit
import CoreLocation

protocol LocationServiceLogic: class {
    func requestWhenInUseAuthorization()
    func locationServicesEnabled() -> Bool
    func getLocation(for listener: LocationListener)
}

protocol LocationListener: class {
    func didReceive(location: CLLocation, address: Address)
}

class LocationServiceManager: UIViewController, LocationServiceLogic {
    
    let locationManager = CLLocationManager()
    weak var delegate: LocationListener?
    
    func requestWhenInUseAuthorization() {
        locationManager.requestWhenInUseAuthorization()
    }
    
    func locationServicesEnabled() -> Bool {
        return CLLocationManager.locationServicesEnabled()
    }
    
    func getLocation(for listener: LocationListener) {
        delegate = listener
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.startUpdatingLocation()
    
    }
    
    func geoCode(location: CLLocation) {
        let geoCoder = CLGeocoder()
        
        geoCoder.reverseGeocodeLocation(location) { [weak self] (placemarks, errors) in
            guard let placemark = placemarks?.last else {
                return
            }
            
            let address = Address(street: placemark.name ?? "",
                                  state: placemark.administrativeArea ?? "",
                                  city: placemark.locality ?? "",
                                  zip: placemark.postalCode ?? "")
            self?.delegate?.didReceive(location: location,
                                       address: address)
        }
    }
    
    func geoCode(address: Address, completion: @escaping (_ location: CLLocation) -> Void) {
        let geoCoder = CLGeocoder()
        
        geoCoder.geocodeAddressString(address.toString) { (placemarks, error) in
            guard let location = placemarks?.last?.location else {
                return
            }
            completion(location)
        }
    }
    
}

extension LocationServiceManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if  let userLocation = locations.last {
            geoCode(location: userLocation)
            locationManager.stopUpdatingLocation()
        }
    }

    
}
