//
//  DayOfTheWeek.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/15/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation

enum DayOfTheWeek: Int {
   
    case sunday
    case monday
    case tuesday
    case wednesday
    case thursday
    case friday
    case saturday
    
    var description: String {
        switch self {
        case .sunday: return "Sun"
        case .monday: return "M"
        case .tuesday: return "T"
        case .wednesday: return "W"
        case .thursday: return "Thu"
        case .friday: return "F"
        case .saturday: return "Sat"
        }
    }
    
    init?(title: String) {
        switch title {
        case "Sun": self = .sunday
        case "M": self = .monday
        case "T": self = .tuesday
        case "W": self = .wednesday
        case "Thu": self = .thursday
        case "F": self = .friday
        case "Sat": self = .saturday
        default: return nil
        }
    }
    
}
