//
//  UserDefaultsService.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 2/17/20.
//  Copyright © 2020 patrickridd. All rights reserved.
//

import Foundation

protocol UserDefaultsDelegate {
    func save(userId: String, email: String, phone: String)
    func save(user: User)
    func getUserData() -> (userId: String?, email: String?, phone: String?)
    func getUser() -> User?
}


struct UserDefaultsService: UserDefaultsDelegate {
    
    let userDefaults = UserDefaults()
    let encoder = JSONEncoder()
    let decoder = JSONDecoder()
    
    let userIdKey = "userId"
    let userEmailKey = "userEmailKey"
    let userPhoneKey = "userPhoneKey"
    let userKey = "userKey"
    
    
    func save(userId: String, email: String, phone: String) {
        userDefaults.set(userId, forKey: userIdKey)
        userDefaults.set(email, forKey: userEmailKey)
        userDefaults.set(phone, forKey: userPhoneKey)
    }

    func save(user: User) {
        if let encoded = try? encoder.encode(user) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: userKey)
        }
    }
    
    func getUserData() -> (userId: String?, email: String?, phone: String?) {
        let userId = userDefaults.string(forKey: userIdKey)
        let email = userDefaults.string(forKey: userEmailKey)
        let phone = userDefaults.string(forKey: userPhoneKey)
        
        return (userId, email, phone)
    }
    
    func getUser() -> User? {
        guard let savedUser = userDefaults.object(forKey: userKey) as? Data else { return nil }
        let decoder = JSONDecoder()
        let user = try? decoder.decode(User.self, from: savedUser)
        return user
    }
    
}
