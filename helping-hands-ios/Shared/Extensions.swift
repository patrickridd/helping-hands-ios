//
//  Extensions.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/15/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

extension Date {
    
    func add(days: Int) -> Date {
        return self.addingTimeInterval(Double(days) * 86400)
    }
    
    func formatDateForDisplay() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        return formatter.string(from: self)
    }
    
    func formatDateAndTimeForDisplay() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yy"
        let dateString = formatter.string(from: self)
        let timeOfDay = self.timeOfDay
        return "\(dateString) • \(timeOfDay)"
    }
    
    var day: Day? {
        let calendar = Calendar.current
        if let dayOfWeek = DayOfTheWeek(rawValue: calendar.component(.weekday, from: self)-1) {
            let day = Day(dayOfTheWeek: dayOfWeek, date: self)
            return day
        } else {
            return nil
        }
    }
    
    var timeOfDay: String {
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: self)
        let minuteInt = calendar.component(.minute, from: self)
        
        let minute: String = minuteInt < 10 ? "0\(minuteInt)" : "\(minuteInt)"
        
        if hour > 12 {
            let updatedHour = hour-12
            return "\(updatedHour):\(minute) PM"
        } else {
            return "\(hour):\(minute) AM"
        }
    }
}

extension PreferredContact: Hashable, Codable {
    
    public func hash(into hasher: inout Hasher) {
        if let hash = Int(self.rawValue) {
            hash.hash(into: &hasher)
        }
    }
}

extension Set {
    
    func toArray() -> Array<Element> {
        return Array(self)
    }
}

extension UIImage {
    
    func toUrl() -> String {
        return self.jpegData(compressionQuality: 1)?.base64EncodedString() ?? ""
    }
    
}

extension String {
    
    func urlToImage() -> UIImage? {
        if let imageData = Data.init(base64Encoded: self, options: .init(rawValue: 0)) {
            return UIImage(data: imageData)
        } else {
            return nil
        }
    }
}
