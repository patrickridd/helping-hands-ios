//
//  RequestListProtocols.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/11/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

protocol RequestListPresenterLogic {
    var view: RequestListViewDelegate? { get set }
    init(view: RequestListViewDelegate)
    
    func getRequests(createdByUser: Bool, for day: Day)
    func cellSelected(at index: Int)
    func hideButtonTapped(at index: Int)
    func acceptButtonTapped(at index: Int)
    func dayButtonTapped(with day: Day?)
    func setupDayButtons()
}

protocol RequestListViewDelegate {
    var presenter: RequestListPresenterLogic! { get set }
    
    func setDataSource(for requests: [Request])
    func updateView(date: Date)
    func acceptButtonTapped(sender: UIButton)
    func hideButtonTapped(sender: UIButton)
    func dayButtonTapped(_ sender: DayButton)
    func present(view: UIViewController)
    func updateDateField(with date: Date) 
    func dayButtonTapped(with day: Day?)
    func setupDayButtons(firstDay: Day?,
                         secondDay: Day?,
                         thirdDay: Day?,
                         fourthDay: Day?,
                         fifthDay: Day?,
                         sixthDay: Day?,
                         seventhDay: Day?)
}

