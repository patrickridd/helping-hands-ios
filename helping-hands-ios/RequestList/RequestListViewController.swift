//
//  RequestListViewController.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/11/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

class RequestListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
   
    @IBOutlet weak var firstDayButton: DayButton!
    @IBOutlet weak var secondDayButton: DayButton!
    @IBOutlet weak var thirdDayButton: DayButton!
    @IBOutlet weak var fourthDayButton: DayButton!
    @IBOutlet weak var fifthDayButton: DayButton!
    @IBOutlet weak var sixthDayButton: DayButton!
    @IBOutlet weak var seventhDayButton: DayButton!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var noOpenRequestsView: UIView!
    @IBOutlet weak var noOpenRequestsLabel: UILabel!
    
    let datePicker = UIDatePicker()

    var presenter: RequestListPresenterLogic!
    
    var requests: [Request] = []
    
    var viewDate: Date = Date()
    
    var isUserRequestList: Bool {
        return presentingViewController == nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter = RequestListPresenter(view: self)
        presenter.setupDayButtons()
        tableView.dataSource = self
        tableView.delegate = self

        let requestCell = UINib(nibName: "RequestCell", bundle: nil)
        tableView.register(requestCell, forCellReuseIdentifier: "RequestCell")
        
        dateTextField.inputAccessoryView = KeyboardToolbar(textField: dateTextField)
        datePicker.datePickerMode = .date
        datePicker.minimumDate = Date()
        datePicker.addTarget(self, action: #selector(updateDateField(sender:)), for: .valueChanged)
        dateTextField.inputView = datePicker
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter.getRequests(createdByUser: isUserRequestList, for: Date().day!)
    }
    
    func setupDayButtons(firstDay: Day?,
                         secondDay: Day?,
                         thirdDay: Day?,
                         fourthDay: Day?,
                         fifthDay: Day?,
                         sixthDay: Day?,
                         seventhDay: Day?) {
        firstDayButton.updateWith(day: firstDay, buttonTag: 0, isSelected: true)
        secondDayButton.updateWith(day: secondDay, buttonTag: 1)
        thirdDayButton.updateWith(day: thirdDay, buttonTag: 2)
        fourthDayButton.updateWith(day: fourthDay, buttonTag: 3)
        fifthDayButton.updateWith(day: fifthDay, buttonTag: 4)
        sixthDayButton.updateWith(day: sixthDay, buttonTag: 5)
        seventhDayButton.updateWith(day: seventhDay, buttonTag: 6)
    }
    
    func dayButtonTapped(with day: Day?) {
        firstDayButton.button(isSelected: day == firstDayButton.day)
        secondDayButton.button(isSelected: day == secondDayButton.day)
        thirdDayButton.button(isSelected: day == thirdDayButton.day)
        fourthDayButton.button(isSelected: day == fourthDayButton.day)
        fifthDayButton.button(isSelected: day == fifthDayButton.day)
        sixthDayButton.button(isSelected: day == sixthDayButton.day)
        seventhDayButton.button(isSelected: day == seventhDayButton.day)
    }
    
    @IBAction func dayButtonTapped(_ sender: DayButton) {
        switch sender.tag {
        case 0: presenter.dayButtonTapped(with: firstDayButton.day)
        case 1: presenter.dayButtonTapped(with: secondDayButton.day)
        case 2: presenter.dayButtonTapped(with: thirdDayButton.day)
        case 3: presenter.dayButtonTapped(with: fourthDayButton.day)
        case 4: presenter.dayButtonTapped(with: fifthDayButton.day)
        case 5: presenter.dayButtonTapped(with: sixthDayButton.day)
        case 6: presenter.dayButtonTapped(with: seventhDayButton.day)
        default: break
        }
    }
    
    @objc func updateDateField(sender: UIDatePicker) {
        dateTextField.text = sender.date.formatDateForDisplay()
        let calendar = Calendar.current

        if let dayOfWeek = DayOfTheWeek(rawValue: calendar.component(.weekday, from: sender.date)-1) {
            let daySelected = Day(dayOfTheWeek: dayOfWeek, date: sender.date)
            presenter.dayButtonTapped(with: daySelected)
        }
    }
    
    func updateDateField(with date: Date) {
        dateTextField.text = date.formatDateForDisplay()
    }
    
    func updateView(date: Date) {
        viewDate = date
        noOpenRequestsLabel.text = "You have no requests open for \(date.formatDateForDisplay())"
        tableView.reloadData()
    }
}

extension RequestListViewController: RequestListViewDelegate {
    
    func setDataSource(for requests: [Request]) {
        self.requests = requests
        self.noOpenRequestsView.isHidden = requests.count != 0
        tableView.reloadData()
    }
    
    func present(view: UIViewController) {
        self.navigationController?.pushViewController(view,
                                                      animated: true)
    }
    
    @objc func acceptButtonTapped(sender: UIButton) {
        presenter.acceptButtonTapped(at: sender.tag)
    }
    
    @objc func hideButtonTapped(sender: UIButton) {
        presenter.hideButtonTapped(at: sender.tag)
    }
}

extension RequestListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return requests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let requestCell = tableView.dequeueReusableCell(withIdentifier: "RequestCell",
                                                        for: indexPath) as? RequestTableViewCell
        let request = requests[indexPath.row]
        requestCell?.update(with: request, for: User())
        requestCell?.set(tag: indexPath.row)
        requestCell?.acceptButton.addTarget(self, action: #selector(acceptButtonTapped(sender:)), for: .touchUpInside)
        requestCell?.hideButton.addTarget(self, action: #selector(hideButtonTapped(sender:)), for: .touchUpInside)
        
        return requestCell ?? UITableViewCell()
    }
    
}

extension RequestListViewController: UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewDate.formatDateForDisplay()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 175
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.cellSelected(at: indexPath.row)
    }
    
    
}
