//
//  RequestListPresenter.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/11/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation

class RequestListPresenter: RequestListPresenterLogic {
    
    var view: RequestListViewDelegate?
    var fetchedRequests: [Request] = []
    var filteredRequests: [Request] = []
    required init(view: RequestListViewDelegate) {
        self.view = view
    }
    
    func getRequests(createdByUser: Bool, for day: Day) {
        self.fetchedRequests =
            [Request(id: "", requester: User(), title: "Clean Car", description: "I have vacuum and other necessary cleaning supplies", latitude: 0.0, longitude: 0.0, address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil), duration: "3 hours", requestDate: Date().add(days: 2)),
             Request(id: "", requester: User(), title: "Laundry", description: "I have detergent and the clothes separated", latitude: 0.0, longitude: 0.0, address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil), duration: "2 hours", requestDate: Date().add(days: 2)),
             Request(id: "", requester: User(), title: "Drive to airport", description: "Thank you.", latitude: 0.0, longitude: 0.0, address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil), duration: "45 minutes", requestDate: Date()),
             Request(id: "", requester: User(), title: "Help clean up yard",description: "I need the lawn mowed and some leaves raked", latitude: 0.0, longitude: 0.0, address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil), duration: "3 hours", requestDate: Date()),
             Request(id: "", requester: User(), title: "Need help with dinner", description: "I have a child who is allergic to legumes so I ask that we avoid anything with those.", latitude: 0.0, longitude: 0.0, address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil), duration: "3 hours", requestDate: Date()),
             Request(id: "", requester: User(), title: "Vacuum",description: "I don't have a vacuum so I would need someone to supply one", latitude: 0.0, longitude: 0.0, address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil), duration: "3 hours", requestDate: Date()),
             Request(id: "", requester: User(), title: "Help clean kitchen", description: "I have all of the cleaning supplies", latitude: 0.0, longitude: 0.0, address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil), duration: "3 hours", requestDate: Date().add(days: 9)),
             Request(id: "", requester: User(), title: "Shovel Snow", description: "I have two shovels and also have salt", latitude: 0.0, longitude: 0.0, address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil), duration: "1 hours", requestDate: Date().add(days: 3)),
             Request(id: "", requester: User(), title: "Mow Lawn", description: "Description", latitude: 0.0, longitude: 0.0, address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil), duration: "3 hours", requestDate: Date().add(days: 5))]
        
        self.filteredRequests = fetchedRequests.filter { (req) -> Bool in
            return req.requestDate.day == day
        }
        
        view?.setDataSource(for: filteredRequests)
    }
    
    func cellSelected(at index: Int) {
        let request = filteredRequests[index]
        let requestDetailView = RequestDetailViewController(nibName: "RequestDetail", bundle: nil)
        requestDetailView.request = request
        view?.present(view: requestDetailView)
    }
    
    func acceptButtonTapped(at index: Int) {
        filteredRequests[index].potentialHelpers.append(User())
        view?.setDataSource(for: filteredRequests)
    }
    
    func hideButtonTapped(at index: Int) {
        filteredRequests.remove(at: index)
        view?.setDataSource(for: filteredRequests)
    }
    
    func setupDayButtons() {
        let firstDate = Date()
        let secondDate = firstDate.add(days: 1)
        let thirdDate = firstDate.add(days: 2)
        let fourthDate = firstDate.add(days: 3)
        let fifthDate = firstDate.add(days: 4)
        let sixthDate = firstDate.add(days: 5)
        let seventhDate = firstDate.add(days: 6)
        let calendar = Calendar.current
        
        if
            let firstDayOfWeek = DayOfTheWeek(rawValue: calendar.component(.weekday, from: firstDate)-1),
            let secondDayOfWeek = DayOfTheWeek(rawValue: calendar.component(.weekday, from: secondDate)-1),
            let thirdDayOfWeek = DayOfTheWeek(rawValue: calendar.component(.weekday, from: thirdDate)-1),
            let fourthDayOfWeek = DayOfTheWeek(rawValue: calendar.component(.weekday, from: fourthDate)-1),
            let fifthDayOfWeek = DayOfTheWeek(rawValue: calendar.component(.weekday, from: fifthDate)-1),
            let sixthDayOfWeek = DayOfTheWeek(rawValue: calendar.component(.weekday, from: sixthDate)-1),
            let seventhDayOfWeek = DayOfTheWeek(rawValue: calendar.component(.weekday, from: seventhDate)-1) {
            view?.updateDateField(with: firstDate)
            view?.setupDayButtons(firstDay: Day(dayOfTheWeek: firstDayOfWeek, date: firstDate),
                                  secondDay: Day(dayOfTheWeek: secondDayOfWeek, date: secondDate),
                                  thirdDay: Day(dayOfTheWeek: thirdDayOfWeek, date: thirdDate),
                                  fourthDay: Day(dayOfTheWeek: fourthDayOfWeek, date: fourthDate),
                                  fifthDay: Day(dayOfTheWeek: fifthDayOfWeek, date: fifthDate),
                                  sixthDay: Day(dayOfTheWeek: sixthDayOfWeek, date: sixthDate),
                                  seventhDay: Day(dayOfTheWeek: seventhDayOfWeek, date: seventhDate))
        }
    }
    
    func dayButtonTapped(with day: Day?) {
        view?.dayButtonTapped(with: day)
        if let day = day {
            getRequests(createdByUser: true, for: day)
            
            view?.updateDateField(with: day.date)
            view?.updateView(date: day.date)
        }
    }
}
