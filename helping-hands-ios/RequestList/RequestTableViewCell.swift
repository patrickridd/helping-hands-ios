//
//  RequestTableViewCell.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/13/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

class RequestTableViewCell: UITableViewCell {

    @IBOutlet weak var requestTitleLabel: UILabel!
    @IBOutlet weak var distanceValueLabel: UILabel!
    @IBOutlet weak var durationValueLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var buttonStatusLabel: UILabel!
    
    @IBOutlet weak var buttonStack: UIStackView!
    @IBOutlet weak var hideButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func update(with request: Request, for user: User) {
        requestTitleLabel.text = request.title
        timeLabel.text = request.requestDate.formatDateForDisplay()
        distanceValueLabel.text = "\(hideButton.tag) miles"
        durationValueLabel.text = request.duration
        buttonStack.isHidden = request.potentialHelpers.contains(user)
        buttonStatusLabel.isHidden = !(request.potentialHelpers.contains(user) || request.helper == user)
        
        if request.potentialHelpers.contains(user) && request.helper == user {
            buttonStatusLabel.text = "** Accepted **"
        } else if request.potentialHelpers.contains(user) {
            buttonStatusLabel.text = "** Waiting on Response **"
        }
    }

    func set(tag: Int) {
        hideButton.tag = tag
        acceptButton.tag = tag
    }
}
