//
//  MainTabBarProtocols.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/8/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

protocol MainTabBarViewDelegate {
    var presenter: MainTabBarPresenterLogic! { get set }
    func addChild(view: UIViewController)
    func present(view: UIViewController)
    func presentRegisterView()
    func menuButtonTapped()
    func handleMenuToggle()
    func setUpTabBarNavigation()
    func error(error: Error)
}

protocol MainTabBarPresenterLogic {
    var view: MainTabBarViewDelegate? { get set }
    
    init(view: MainTabBarViewDelegate)
    func setUpTabBarNavigation()
    func menuButtonTapped()
    func fetchUserProfile() 
}
