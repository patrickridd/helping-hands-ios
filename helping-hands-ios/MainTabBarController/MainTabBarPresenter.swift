//
//  MainTabBarPresenter.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/8/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation
import AWSAppSync

class MainTabBarPresenter: MainTabBarPresenterLogic {
    
    let userDefaults: UserDefaultsDelegate = UserDefaultsService()
    let appSyncService: AppSyncServiceDelegate = AppSyncService()
    var view: MainTabBarViewDelegate?
    var user: User?
    var appSyncClient: AWSAppSyncClient?
    
    required init(view: MainTabBarViewDelegate) {
        self.view = view
        self.appSyncClient = (UIApplication.shared.delegate as! AppDelegate).appSyncClient
    }
    
    func setUpTabBarNavigation() {
        view?.setUpTabBarNavigation()
    }
    
    func fetchUserProfile() {
        if let userId = userDefaults.getUserData().userId,
            let userEmail = userDefaults.getUserData().email,
            let userPhone = userDefaults.getUserData().phone {
            let userQuery = GetUserQuery(id: userId)
            appSyncClient?.fetch(query: userQuery, cachePolicy: .returnCacheDataAndFetch, queue: .main, resultHandler: { [weak self] (result, _) in
                
                if let userData = result?.data?.getUser,
                   let user = User(userData: userData) {
                    // Initialize local user and cache it
                    self?.userDefaults.save(user: user)
                } else {
                    // Else if no User profile found, create backend User and present profile options
                    self?.createInitialUser(with: userId, email: userEmail, phone: userPhone)
                }
            })
        }
    }
    
    func menuButtonTapped() {
        view?.handleMenuToggle()
    }
    
    func createInitialUser(with id: String, email: String, phone: String) {
        appSyncService.createUser(with: id, email: email, phone: phone) { [weak self] (error) in
            print(error)
            self?.view?.presentRegisterView()
        }
    }
}
