//
//  MainTabBarViewController.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/8/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController {
    
    var presenter: MainTabBarPresenterLogic!
   
    weak var menuDrawerDelegate: MenuDrawerDelegate?
    
    
    lazy var hamburgerButton: UIBarButtonItem = {
        let image = UIImage(systemName: "line.horizontal.3")
        let button = UIBarButtonItem(image: image,
                                     style: .done,
                                     target: self,
                                     action: #selector(menuButtonTapped))
        return button
    }()
    
    lazy var homeItem: UITabBarItem = {
        let image = UIImage(systemName: "heart.circle")
        let selectedImage = UIImage(systemName: "heart.circle.fill")
        let item = UITabBarItem(title: "Help",
                                image: image,
                                selectedImage: selectedImage)
        return item
    }()
    
    lazy var myRequestsItem: UITabBarItem = {
        let image = UIImage(systemName: "person.circle")
        let selectedImage = UIImage(systemName: "person.circle.fill")
        let item = UITabBarItem(title: "You",
                                image: image,
                                selectedImage: selectedImage)
        return item
    }()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        presenter = MainTabBarPresenter(view: self)
        
        definesPresentationContext = true
        presenter.setUpTabBarNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.hidesBackButton = true
        navigationController?.navigationBar.isHidden = false
        navigationItem.leftBarButtonItem = hamburgerButton

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        presenter.fetchUserProfile()
    }
    
}

extension MainTabBarViewController: MainTabBarViewDelegate {
    
    func addChild(view: UIViewController) {
        self.addChild(view)
    }
    
    @objc func present(view: UIViewController) {
        present(view, animated: true, completion: nil)
    }
    
    @objc func menuButtonTapped() {
        presenter.menuButtonTapped()
    }
    
    func handleMenuToggle() {
        menuDrawerDelegate?.handleMenuToggle(for: nil)
    }
    
    func error(error: Error) {
        print(error.localizedDescription)
    }
    
    func setUpTabBarNavigation() {
        // Set HomeView -
        let homeView = HomeViewController(nibName: "Home", bundle: nil)
        homeView.tabBarItem = homeItem
        self.addChild(view: homeView)
        
        // Set MyRequests -
        let myRequestsStoryboard = UIStoryboard(name: "MyRequests", bundle: nil)
        if let myRequestsView = myRequestsStoryboard.instantiateViewController(identifier: "MyRequestsView") as? MyRequestsViewController {
            myRequestsView.tabBarItem = myRequestsItem
            self.addChild(view: myRequestsView)
        }
    }
    
    func presentRegisterView() {
        let registerView = RegisterChoiceViewController(nibName: "RegisterChoice", bundle: nil)
        let navController = UINavigationController(rootViewController: registerView)
        navController.modalPresentationStyle = .fullScreen
        present(navController, animated: true, completion: nil)
    }
}
