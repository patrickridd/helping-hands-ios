//
//  RequestDetailViewController.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/13/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

class RequestDetailViewController: UIViewController {
    
    @IBOutlet weak var requestTitle: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    var request: Request!
    var presenter: RequestDetailPresenterLogic!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter = RequestDetailPresenter(view: self, presentedModally: navigationController == nil)
        presenter.loadView(for: request)
        acceptButton.addTarget(self, action: #selector(acceptButtonTapped), for: .touchUpInside)
        cancelButton.addTarget(self, action: #selector(cancelButtonTapped), for: .touchUpInside)
    }

}

extension RequestDetailViewController: RequestDetailViewDelegate {
    
    func loadView(for request: Request) {
        requestTitle.text = request.title
        dateLabel.text = request.requestDate.formatDateForDisplay()
        timeLabel.text = request.requestDate.timeOfDay
        descriptionLabel.text = request.description
        durationLabel.text = request.duration
        addressLabel.text = request.address?.toString ?? "No Address Listed"
        cancelButton.isHidden = !request.potentialHelpers.contains(User())
        acceptButton.isHidden = request.potentialHelpers.contains(User())
    }
    
    @objc func acceptButtonTapped() {
        presenter.acceptButtonTapped()
    }
    
    @objc func cancelButtonTapped() {
        presenter.cancelButtonTapped()
    }
    
    func popView() {
        self.navigationController?.popViewController(animated: true)
    }

    func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
}
