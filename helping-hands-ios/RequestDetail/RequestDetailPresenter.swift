//
//  RequestDetailPresenter.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/13/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation

class RequestDetailPresenter: RequestDetailPresenterLogic {
    
    var view: RequestDetailViewDelegate?
    var request: Request?
    var presentedModally: Bool
    
    required init(view: RequestDetailViewDelegate, presentedModally: Bool) {
        self.view = view
        self.presentedModally = presentedModally
    }
    
    func loadView(for request: Request) {
        self.request = request
        view?.loadView(for: request)
    }
    
    func acceptButtonTapped() {
        request?.potentialHelpers.append(User())
        if presentedModally {
            view?.dismissView()
        } else {
            view?.popView()
        }
    }
    
    func cancelButtonTapped() {
        if let index = request?.potentialHelpers.firstIndex(of: User()) {
            request?.potentialHelpers.remove(at: index)
            if presentedModally {
                view?.dismissView()
            } else {
                view?.popView()
            }
        }
    }

}
