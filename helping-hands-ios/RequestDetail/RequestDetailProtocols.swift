//
//  RequestDetailProtocols.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/13/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation

protocol RequestDetailPresenterLogic {
    var view: RequestDetailViewDelegate? { get set }
    var presentedModally: Bool { get set }
    
    var request: Request? { get set }
    
    init(view: RequestDetailViewDelegate, presentedModally: Bool)

    func loadView(for request: Request)
    func acceptButtonTapped()
    func cancelButtonTapped()
}

protocol RequestDetailViewDelegate {
    var request: Request! { get set }
    var presenter: RequestDetailPresenterLogic! { get set }
    
    func loadView(for request: Request)
    func acceptButtonTapped()
    func cancelButtonTapped()
    func popView()
    func dismissView()
}
