//
//  ContactPresenter.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/5/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation
import AWSAppSync

class ContactPresenter: ContactPresenterLogic {
    
    let userDefaults: UserDefaultsDelegate = UserDefaultsService()
    let appSyncService: AppSyncServiceDelegate = AppSyncService()
    var view: ContactViewDelegate?
    var appSyncClient: AWSAppSyncClient?
    
    required init(view: ContactViewDelegate) {
        self.view = view
        self.appSyncClient = (UIApplication.shared.delegate as! AppDelegate).appSyncClient
    }
    
    func backButtonTapped() {
        view?.popView()
    }
    
    func nextButtonTapped(firstName: String?, lastName: String?, preferredContact: Set<PreferredContact>) {
        guard
            let firstName = firstName, firstName != "",
            let lastName = lastName, lastName != ""
        else {
            return
        }
        
        updateUser(with: firstName,
                   and: lastName,
                   preferredContact: preferredContact)
        
        view?.navigateToAboutMeView()
    }
    
    func skipButtonTapped() {
        view?.dismissView()
    }
    
    func updateUser(with firstName: String, and lastName: String, preferredContact: Set<PreferredContact>) {
        guard let user = userDefaults.getUser() else {
            return
        }
        user.firstName = firstName
        user.lastName = lastName
        user.preferredContact = preferredContact.toArray()
        userDefaults.save(user: user)
        
        appSyncService.update(user: user) { (error) in
            print(error)
        }
    }
    
    func updateView() {
        if let user = userDefaults.getUser() {
            view?.updateView(with: user)
        }
    }
}
