//
//  ContactProtocols.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/5/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation

protocol ContactViewDelegate {
    var presenter: ContactPresenterLogic! { get set }
    
    func backButtonTapped()
    func nextButtonTapped()
    func skipButtonTapped()
    func popView()
    func dismissView()
    func navigateToAboutMeView()
    func updateView(with user: User)
}

protocol ContactPresenterLogic {
    var view: ContactViewDelegate? { get set }
    
    init(view: ContactViewDelegate)
    func backButtonTapped()
    func nextButtonTapped(firstName: String?, lastName: String?, preferredContact: Set<PreferredContact>)
    func skipButtonTapped()
    func updateView()
    
}
