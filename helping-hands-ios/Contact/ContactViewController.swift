//
//  ContactViewController.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/5/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController, ContactViewDelegate {
    
    var presenter: ContactPresenterLogic!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var nextPageButton: UIButton!
    
    @IBOutlet weak var callSwitch: UISwitch!
    @IBOutlet weak var textSwitch: UISwitch!
    @IBOutlet weak var emailSwitch: UISwitch!
 
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter = ContactPresenter(view: self)
        
        firstNameTextField.inputAccessoryView = KeyboardToolbar(textField: firstNameTextField)
        lastNameTextField.inputAccessoryView = KeyboardToolbar(textField: lastNameTextField)

        backButton.addTarget(self,
                             action: #selector(backButtonTapped),
                             for: .touchUpInside)
        skipButton.addTarget(self,
                             action: #selector(skipButtonTapped),
                             for: .touchUpInside)
        nextPageButton.addTarget(self,
                                 action: #selector(nextButtonTapped),
                                 for: .touchUpInside)
        presenter.updateView()
    }
    
}

extension ContactViewController {

    @objc func backButtonTapped() {
        presenter.backButtonTapped()
    }
    
    @objc func skipButtonTapped() {
        presenter.skipButtonTapped()
    }
    
    @objc func nextButtonTapped() {
        var preferredContact: Set<PreferredContact> = []
        if callSwitch.isOn { preferredContact.insert(.call)}
        if textSwitch.isOn { preferredContact.insert(.text)}
        if emailSwitch.isOn { preferredContact.insert(.email)}
        presenter.nextButtonTapped(firstName: firstNameTextField.text,
                                   lastName: lastNameTextField.text,
                                   preferredContact: preferredContact)
    }
    
    func updateView(with user: User) {
        firstNameTextField.text = user.firstName
        lastNameTextField.text = user.lastName
        
        callSwitch.isOn = user.preferredContact.contains(.call)
        textSwitch.isOn = user.preferredContact.contains(.text)
        emailSwitch.isOn = user.preferredContact.contains(.email)
    }
    
    func popView() {
        navigationController?.popViewController(animated: true)
    }
    
    func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func navigateToAboutMeView() {
        let aboutMeView = AboutMeViewController(nibName: "AboutMe", bundle: nil)
        navigationController?.pushViewController(aboutMeView, animated: true)
    }
}
