//
//  AddressViewController.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/6/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

class AddressViewController: UIViewController {

    // TextFields
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var addressTextFieldTwo: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var zipTextField: UITextField!
    
    // Buttons
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var currentLocationButton: UIButton!
    @IBOutlet weak var addressStackCenterYConstraint: NSLayoutConstraint!
    
    
    var request: Request?
    var user: User?
    
    var presenter: AddressPresenterLogic!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter = AddressPresenter(view: self, request: request, user: user)
        
        addressTextField.inputAccessoryView = KeyboardToolbar(textField: addressTextField)
        addressTextFieldTwo.inputAccessoryView = KeyboardToolbar(textField: addressTextFieldTwo)
        cityTextField.inputAccessoryView = KeyboardToolbar(textField: cityTextField)
        stateTextField.inputAccessoryView = KeyboardToolbar(textField: stateTextField)
        zipTextField.inputAccessoryView = KeyboardToolbar(textField: zipTextField)
        
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        doneButton.addTarget(self, action: #selector(doneButtonTapped), for: .touchUpInside)
        currentLocationButton.addTarget(self, action: #selector(currentLocationButtonTapped), for: .touchUpInside)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        presenter.loadData()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension AddressViewController: AddressViewDelegate {
    
    @objc func backButtonTapped() {
        presenter.backButtonTapped()
    }
    
    @objc func doneButtonTapped() {
        guard
            let street = addressTextField.text,
            let zip = zipTextField.text,
            let city = cityTextField.text,
            let state = stateTextField.text
        else {
            return
        }
        
        let address = Address(street: street,
                              state: state,
                              city: city,
                              zip: zip,
                              streetTwo: addressTextFieldTwo.text)
       
        presenter.doneButtonTapped(user: user,
                                   request: request,
                                   and: address)
    }
    
    @objc func currentLocationButtonTapped() {
        presenter.currentLocationButtonTapped()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        UIView.animate(withDuration: 0.1, animations: { [weak self] () -> Void in
            self?.addressStackCenterYConstraint.constant = -100
        })
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.addressStackCenterYConstraint.constant = -50
    }
    
    func popView() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func updateView(with address: Address) {
        addressTextField.text = address.street
        cityTextField.text = address.city
        stateTextField.text = address.state
        zipTextField.text = address.zip
    }
}
