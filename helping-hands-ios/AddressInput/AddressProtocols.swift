//
//  AddressProtocols.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/6/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation

protocol AddressPresenterLogic {
    var view: AddressViewDelegate? { get set }
    var user: User? { get set }
    var request: Request? { get set }
    init(view: AddressViewDelegate, request: Request?, user: User?)
    
    func loadData()
    func backButtonTapped()
    func doneButtonTapped(user: User?, request: Request?, and address: Address)
    func updateInfo(for user: User)
    func updateInfo(for request: Request) 
    func currentLocationButtonTapped()

}

protocol AddressViewDelegate {
    var presenter: AddressPresenterLogic! { get set }
    
    func backButtonTapped()
    func doneButtonTapped()
    func currentLocationButtonTapped()
    func updateView(with address: Address)
    func popView()
    func dismissView()
}
