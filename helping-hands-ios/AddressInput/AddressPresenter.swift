//
//  AddressPresenter.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/6/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation
import CoreLocation
import AWSAppSync

class AddressPresenter: AddressPresenterLogic {
    
    var view: AddressViewDelegate?
    let locationServiceManager = LocationServiceManager()
    var request: Request?
    var user: User?
    var appSyncClient: AWSAppSyncClient?
    
    required init(view: AddressViewDelegate, request: Request?, user: User?) {
        self.view = view
        self.request = request
        self.user = user
        locationServiceManager.delegate = self
        self.appSyncClient = (UIApplication.shared.delegate as! AppDelegate).appSyncClient
    }
    
    func backButtonTapped() {
        view?.popView()
    }
    
    func doneButtonTapped(user: User?, request: Request?, and address: Address) {
        
        locationServiceManager.geoCode(address: address) { [weak self] (location) in
            if let user = user {
                user.address = address
                self?.updateInfo(for: user)
            }
            
            if var request = request {
                request.update(lat: location.coordinate.latitude,
                               long: location.coordinate.longitude)
                self?.updateInfo(for: request)
            }
            self?.view?.dismissView()
        }
    }
      
    func currentLocationButtonTapped() {
        locationServiceManager.requestWhenInUseAuthorization()
        
        if locationServiceManager.locationServicesEnabled() {
            locationServiceManager.getLocation(for: self)
        }
    }
    
    func updateInfo(for user: User) {
        print(user)
    }
    
    func updateInfo(for request: Request) {
        print(request)
    }
    
    func loadData() {
        if let request = request,
           let latitude = CLLocationDegrees(exactly: request.latitude),
           let longitude = CLLocationDegrees(exactly: request.longitude) {
            
            let location = CLLocation(latitude: latitude, longitude: longitude)
            locationServiceManager.geoCode(location: location)
        }
        
        if let address = user?.address {
            view?.updateView(with: address)
        }
    }
    
}

extension AddressPresenter: LocationListener {
   
    func didReceive(location: CLLocation, address: Address) {
        view?.updateView(with: address)
    }
    
}

