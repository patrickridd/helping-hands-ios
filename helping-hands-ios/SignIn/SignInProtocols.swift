//
//  SignInProtocols.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 10/30/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation

import AWSMobileClient

protocol SignInPresenterLogic {
    // Weak Reference (avoid retain cycle)
    var view: SignInViewDelegate? { get set }
    init(view: SignInViewDelegate, model: Any)
    func setAWSUserData()
    func checkIfSignedIn()
    func navigateToContainerView()
}


protocol SignInViewDelegate: class {
    // Strong Reference
    var presenter: SignInPresenterLogic! { get set }
    
    func presentAWSSignIn()
    func checkIfSignedIn()
    func navigate(to containerView: UIViewController)
}

