//
//  SignInViewController.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 10/30/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit
import AWSMobileClient

class SignInViewController: UIViewController {
    
    var presenter: SignInPresenterLogic!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        presenter = SignInPresenter(view: self, model: User())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Check if signed in everytime the view appears because we may be navigated to this screen from signing out
        presenter.checkIfSignedIn()
        self.navigationController?.navigationBar.isHidden = true
    }
    
}


extension SignInViewController: SignInViewDelegate {
    
    func presentAWSSignIn() {
        let color = UIColor(red: 237/255, green: 106/255, blue: 90/255, alpha: 1.0)
        let signInOptions = SignInUIOptions(canCancel: false,
                                            logoImage: nil,
                                            backgroundColor: color,
                                            secondaryBackgroundColor: nil,
                                            primaryColor: nil,
                                            disableSignUpButton: false)
        
        AWSMobileClient.default().showSignIn(navigationController: self.navigationController!,
                                             signInUIOptions:signInOptions) { [weak self] (_, error) in
           if let error = error {
              print(error.localizedDescription)
           }
           self?.navigationController?.popViewController(animated: true)
        }
    }
    
    func navigate(to containerView: UIViewController) {
        self.navigationController?.pushViewController(containerView, animated: true)
    }
    
    func checkIfSignedIn() {
        presenter.checkIfSignedIn()
    }
    
    
}
