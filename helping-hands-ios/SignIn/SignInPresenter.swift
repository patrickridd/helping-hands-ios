//
//  SignInPresenter.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 10/30/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation
import AWSMobileClient
import AWSAppSync

class SignInPresenter: SignInPresenterLogic {
    
    weak var user: User?
    weak var view: SignInViewDelegate?
    var appSyncClient: AWSAppSyncClient?

    let userDefaults: UserDefaultsDelegate
    
    required init(view: SignInViewDelegate, model: Any) {
        self.view = view
        user = model as? User
        self.userDefaults = UserDefaultsService()
        self.appSyncClient = (UIApplication.shared.delegate as! AppDelegate).appSyncClient
    }
    
    func checkIfSignedIn() {
        DispatchQueue.main.async {
            if AWSSignInManager.sharedInstance().isLoggedIn {
                self.setAWSUserData()
                self.navigateToContainerView()
            } else {
                self.view?.presentAWSSignIn()
            }
        }
    }
    
    func navigateToContainerView() {
        let containerView = ContainerViewController()
        view?.navigate(to: containerView)
    }
    
    func setAWSUserData() {
        AWSMobileClient.default().getUserAttributes { [weak self] (attributes, _) in
            if let attributes = attributes,
                let userId = attributes["sub"],
                let email = attributes["email"],
                let phone = attributes["phone_number"] {
                    self?.userDefaults.save(userId: userId, email: email, phone: phone)
            }
        }
    }
    
} 
