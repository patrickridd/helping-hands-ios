//
//  CreateRequestPresenter.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/11/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation

class CreateRequestPresenter: CreateRequestPresenterLogic {
   
    var view: CreateRequestViewDelegate?
    var request: Request?
    
    required init(view: CreateRequestViewDelegate, request: Request?) {
        self.view = view
        self.request = request
    }
    
    func nextButtonTapped(title: String, description: String, date: Date, duration: String) {
        let addressView = AddressViewController(nibName: "Address",
                                                       bundle: nil)
        if let existingRequest = request {
            let updatedRequest = Request(id: existingRequest.id,
                                         requester: User(),
                                         title: title,
                                         description: description,
                                         latitude: existingRequest.latitude,
                                         longitude: existingRequest.longitude,
                                         duration: existingRequest.duration,
                                         requestDate: existingRequest.requestDate)
            addressView.request = updatedRequest
        } else {
            let newRequest = Request(id: UUID().uuidString,
                                     requester: User(),
                                     title: title,
                                     description: description,
                                     latitude: 0,
                                     longitude: 0,
                                     duration: duration,
                                     requestDate: Date())
            addressView.request = newRequest
        }
        view?.present(view: addressView)
    }
    
    func backButtonTapped() {
        view?.popView()
    }
    
    func loadRequestContent() {
        if let request = request {
            view?.setContent(for: request)
        } else {
            view?.setContentForNewRequest()
        }
    }
}
