//
//  CreateRequestViewController.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/11/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

class CreateRequestViewController: UIViewController {

    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var requestTitleTextField: UITextField!
    @IBOutlet weak var requestDescriptionTextView: UITextView!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var durationTextField: UITextField!
    @IBOutlet weak var requestHeaderLabel: UILabel!
    @IBOutlet weak var bottomViewTopConstraint: NSLayoutConstraint!
    
    var presenter: CreateRequestPresenterLogic!
    let datePicker = UIDatePicker()
    var request: Request?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter = CreateRequestPresenter(view: self, request: request)
        nextButton.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        
        datePicker.datePickerMode = .dateAndTime
        datePicker.minimumDate = Date()
        datePicker.addTarget(self, action: #selector(updateDateField(sender:)), for: .valueChanged)
        dateTextField.inputView = datePicker
        dateTextField.text = datePicker.date.formatDateAndTimeForDisplay()
        
        dateTextField.inputAccessoryView = KeyboardToolbar(textField: dateTextField)
        durationTextField.inputAccessoryView = KeyboardToolbar(textField: durationTextField)
        requestDescriptionTextView.inputAccessoryView = KeyboardToolbar(textView: requestDescriptionTextView)
        requestTitleTextField.inputAccessoryView = KeyboardToolbar(textField: requestTitleTextField)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        
        navigationController?.navigationBar.isHidden = true
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter.loadRequestContent()
    }

    // Called when the date picker changes.

    @objc func updateDateField(sender: UIDatePicker) {
        dateTextField.text = sender.date.formatDateAndTimeForDisplay()
    }
    
    @objc func resignTextFields() {
        dateTextField.resignFirstResponder()
        durationTextField.resignFirstResponder()
        requestDescriptionTextView.resignFirstResponder()
        requestTitleTextField.resignFirstResponder()
    }
}

extension CreateRequestViewController: CreateRequestViewDelegate {
    
    @objc func nextButtonTapped() {
        presenter.nextButtonTapped(title: requestTitleTextField.text ?? "",
                                   description: requestDescriptionTextView.text,
                                   date: datePicker.date,
                                   duration: durationTextField.text ?? "")
    }
    
     @objc func keyboardWillShow(notification: NSNotification) {
        if requestTitleTextField.isFirstResponder {
            return
        }
        
        UIView.animate(withDuration: 0.1, animations: { [weak self] () -> Void in
            guard let self = self else { return }
            if self.dateTextField.isFirstResponder {
                self.bottomViewTopConstraint.constant = 50
            } else {
                self.bottomViewTopConstraint.constant = 175
            }
        })
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        self.bottomViewTopConstraint.constant = 0
    }
    
    @objc func backButtonTapped() {
        presenter.backButtonTapped()
    }
    
    func present(view: UIViewController) {
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    func popView() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setContent(for request: Request) {
        dateTextField.text = request.requestDate.formatDateAndTimeForDisplay()
        durationTextField.text = request.duration
        requestDescriptionTextView.text = request.description
        requestTitleTextField.text = request.title
        requestHeaderLabel.text = "Modify Request"
        backButton.isHidden = false
    }
    
    func setContentForNewRequest() {
        dateTextField.text = Date().formatDateAndTimeForDisplay()
        durationTextField.text = ""
        requestDescriptionTextView.text = ""
        requestTitleTextField.text = ""
        requestHeaderLabel.text = "New Request"
        backButton.isHidden = true
    }
    
}
