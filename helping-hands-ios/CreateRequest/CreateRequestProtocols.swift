//
//  CreateRequestProtocols.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/11/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

protocol CreateRequestViewDelegate {
    var presenter: CreateRequestPresenterLogic! { get set }
    var request: Request? { get set }
    func nextButtonTapped()
    func backButtonTapped()
    func present(view: UIViewController)
    func popView()
    func setContentForNewRequest()
    func setContent(for request: Request)
}

protocol CreateRequestPresenterLogic {
    var view: CreateRequestViewDelegate? { get set }
    var request: Request? { get set }
    init(view: CreateRequestViewDelegate, request: Request?)
    
    func nextButtonTapped(title: String, description: String, date: Date, duration: String)
    func backButtonTapped()
    func loadRequestContent()
}
