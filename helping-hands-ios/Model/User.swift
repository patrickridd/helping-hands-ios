//
//  User.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 10/30/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation
import UIKit

class User: Hashable, Codable {
    
    static func == (lhs: User, rhs: User) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        if let hash = Int(id) {
            hash.hash(into: &hasher)
        }
    }
    
    let id: String
    
    var helperRating: Double?
    
    var firstName: String?
    var lastName: String?
    
    let phoneNumber: String
    let email: String
    var description: String?
    
    let helperRatings: [Double]
    
    var preferredContact: [PreferredContact] = []
    
    var address: Address?
    
    var fullName: String {
        guard
            let first = firstName,
            let last = lastName
        else {
            return ""
        }
        return first + " " + last
    }
    
    var userNeedsToRegister: Bool {
        return firstName == nil
    }
    
    var imageUrl: String?
    
    init(id: String = UUID().uuidString, username: String, firstName: String, lastName: String, rating: Double? = nil, address: Address? = nil) {
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.address = address
        self.helperRating = rating
        self.helperRatings = []
        self.phoneNumber = ""
        self.email = ""
        self.description = ""
    }
    
    init?(userData: GetUserQuery.Data.GetUser?) {
        guard
            let id = userData?.id,
            let phoneNumber = userData?.phoneNumber,
            let email = userData?.email
        else {
            return nil
        }
        
        self.helperRatings = userData?.helperRatings as? [Double] ?? []
        self.id = id
        self.firstName = userData?.firstName
        self.lastName = userData?.lastName
        self.description = userData?.description
        self.phoneNumber = phoneNumber
        self.email = email
        self.preferredContact = userData?.preferredContact as? [PreferredContact] ?? []
        self.imageUrl = userData?.imageUrl
    }
    
    init() {
        id = "ID"
        firstName = ""
        lastName = ""
        phoneNumber = ""
        email = ""
        description = ""
        helperRatings = []
    }
}
