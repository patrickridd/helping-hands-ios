//
//  Day.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/15/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation

struct Day: Equatable {
    
    static func ==(lhs: Day, rhs: Day)-> Bool {
        return (lhs.dayOfTheWeek == rhs.dayOfTheWeek) &&
               (Calendar.current.component(.day, from: lhs.date) == Calendar.current.component(.day, from: rhs.date)) &&
               (Calendar.current.component(.month, from: lhs.date) == Calendar.current.component(.month, from: rhs.date))
    }
    
    let dayOfTheWeek: DayOfTheWeek
    let date: Date
    
    
}
