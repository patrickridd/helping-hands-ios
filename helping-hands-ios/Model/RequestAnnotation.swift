//
//  RequestAnnotation.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/22/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit
import MapKit

class RequestAnnotation: NSObject, MKAnnotation {
    
    let request: Request
    var title: String?
    var subtitle: String?
    var coordinate: CLLocationCoordinate2D
    
    
    init(coordinate: CLLocationCoordinate2D, request: Request) {
        self.coordinate = coordinate
        self.request = request
        super.init()
    }
    
}
