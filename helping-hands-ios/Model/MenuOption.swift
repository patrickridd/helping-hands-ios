//
//  MenuOption.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 12/5/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

enum MenuOption: Int {
    
    case profile
    case logOut
    
    var description: String {
        switch self {
        case .profile:
            return "Profile"
        case .logOut:
            return "Log Out"
        }
    }
    
    var image: UIImage {
        switch self {
        case .profile:
            return UIImage(systemName: "person") ?? UIImage()
        case .logOut:
            return UIImage(systemName: "arrow.left") ?? UIImage()
        }
    }
    
    var buttonColor: UIColor {
        switch self {
        case .profile:
            return .systemBlue
        case .logOut:
            return .systemRed
        }
    }
    
    static var count: Int {
        var total = 0
        while(MenuOption(rawValue: total) != nil) {
            total += 1
        }
        return total
    }
}
