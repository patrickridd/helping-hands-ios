//
//  Address.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/7/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation

struct Address: Codable {
    
    let street: String
    let state: String
    let city: String
    let zip: String
    var streetTwo: String?
    
    var toString: String {
        return street + ", \n" + city + ", " + state + ", " + zip
    }
}
