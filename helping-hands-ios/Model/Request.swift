//
//  Request.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/11/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation
import CoreLocation

class RequestSection {
    var requestDate: Date
    var requests: [Request]
    
    init(requestDate: Date, requests: [Request]) {
        self.requestDate = requestDate
        self.requests = requests
    }
}

struct Request {
    
    let id: String
    let requester: User
    let title: String
    let description: String
    var latitude: Double
    var longitude: Double
    var address: Address?
    var duration: String
    var requestDate: Date = Date()
    
    var potentialHelpers: [User] = []
    var blockedUsers: Set<User> = []
    
    var helper: User?
    
    var location: CLLocation {
        return CLLocation(latitude: CLLocationDegrees(latitude),
                          longitude: CLLocationDegrees(longitude))
    }
    
    mutating func update(lat: Double?, long: Double?) {
        if let lat = lat {
            self.latitude = lat
        }
        
        if let long = long {
            self.longitude = long
        }
    }
}
