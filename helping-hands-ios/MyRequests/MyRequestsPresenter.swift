//
//  MyRequestsPresenter.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/14/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import Foundation

class MyRequestsPresenter: MyRequestsPresenterLogic {
   
    var view: MyRequestsViewDelegate?
    var requestSections: [RequestSection] = []
    
    required init(view: MyRequestsViewDelegate) {
        self.view = view
    }

    func newRequestButtonTapped() {
        view?.presentCreateRequestView(with: nil)
    }
    
    func getRequestSections() {
        let helper = User(username: "TheO's", firstName: "Theo", lastName: "Von")
        
        let requests =
            [Request(id: UUID().uuidString,
                     requester: User(),
                     title: "Laundry",
                     description: "Just need the whites washed",
                     latitude: 40.757307,
                     longitude: -111.882016,
                     address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil),
                     duration: "2 hours",
                     requestDate: Date().add(days: 1),
                     potentialHelpers: [User(username: "", firstName: "Pete", lastName: "Ridd", rating: 5.0),
                                        User(username: "", firstName: "Drew", lastName: "Breeze", rating: nil)]),
             Request(id: UUID().uuidString,
                     requester: User(),
                     title: "Lawn Mowed",
                     description: "I have a lawn mower. Thank you!",
                     latitude: 40.757307,
                     longitude: -111.882016,
                     address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil),
                     duration: "1.5 hours",
                     requestDate: Date().add(days: 3),
                     potentialHelpers: [User(username: "", firstName: "Pete", lastName: "Ridd", rating: 5.0)]),
             Request(id: UUID().uuidString,
                     requester: User(),
                     title: "Leaves Raked",
                     description: "I just need the backyard done. I have a rake and a leaf blower",
                     latitude: 40.757307,
                     longitude: -111.882016,
                     address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil),
                     duration: "2 hours",
                     requestDate: Date()),
             Request(id: UUID().uuidString,
                     requester: User(),
                     title: "Groceries",
                     description: "I'll text you the list. Thanks :D",
                     latitude: 40.757307,
                     longitude: -111.882016,
                     address: Address(street: "100 East 100 North", state: "Utah", city: "Salt Lake City", zip: "84112", streetTwo: nil),
                     duration: "2 hours",
                     requestDate: Date().add(days: 3),
                     potentialHelpers: [User(username: "", firstName: "Bryan", lastName: "Callen", rating: 2.7)],
                     helper: helper)]
            
        self.requestSections = createSections(for: requests)
        view?.set(requestSections: requestSections)
    }
    
    func createSections(for requests: [Request]) -> [RequestSection] {
        
        let sortedRequests = requests.sorted { (r1, r2) -> Bool in
            return r1.requestDate < r2.requestDate
        }
        
        
        var requestSections: [RequestSection] = []
        
        for request in sortedRequests {
            
            // If this is the first request - create the first request section and append it with the request
            if request.id == sortedRequests.first?.id {
                let requestSection = RequestSection(requestDate: request.requestDate,
                                                    requests: [request])
                requestSections.append(requestSection)
                continue
            }
            
            // Loop through RequestSections and see if the date matches the request and append it if it does
            for requestSection in requestSections {
                if request.requestDate.day == requestSection.requestDate.day {
                    requestSection.requests.append(request)
                    continue
                }
            }
            
            // Check if we appended request in section
            let requestInSections = requestSections.compactMap({ $0.requests.contains { (req) -> Bool in
                return req.id == request.id} }).contains(true)
            
            // If we never appended the request to a request section, create a new request section with the request
            if !requestInSections {
                let requestSection = RequestSection(requestDate: request.requestDate,
                                                                  requests: [request])
                requestSections.append(requestSection)
                continue
            }
        }
        
        return requestSections
    }
    
    func modifyButtonTapped(at indexPath: IndexPath) {
        let section = self.requestSections[indexPath.section]
        let request = section.requests[indexPath.row]
        view?.presentCreateRequestView(with: request)
    }
    
    func cancelButtonTapped(at indexPath: IndexPath) {
        view?.presentDeleteRequestAlert(for: indexPath)
    }
    
    func minusHelperButtonTapped(at indexPath: IndexPath) {
        
        // Extract helper and put them in potentialHelpers array
        if let helper = requestSections[indexPath.section].requests[indexPath.row].helper {
            requestSections[indexPath.section].requests[indexPath.row].potentialHelpers.append(helper)
        }
        
        // Set request helper to nil
        requestSections[indexPath.section].requests[indexPath.row].helper = nil
        
        // Update datasource
        view?.set(requestSections: requestSections)
        
        // TODO: Update backend
    }
    
    func removeRequest(at indexPath: IndexPath) {
        let section = self.requestSections[indexPath.section]
       
        // Remove Request in section
        let request = section.requests.remove(at: indexPath.row)
        
        // Remove section if there are no longer requests within it
        if section.requests.isEmpty {
            requestSections.remove(at: indexPath.section)
        }
        
        // Update view's datasource
        self.view?.set(requestSections: self.requestSections)
        
        // TODO: Remove request on backend belowUpdate backend  -
        print("Removing \(request)")
    }
    
    func didBlockHelper(requestIndexPath: IndexPath, helperIndexPath: IndexPath) {
        let blockedHelper = requestSections[requestIndexPath.section].requests[requestIndexPath.row].potentialHelpers.remove(at: helperIndexPath.row)
        requestSections[requestIndexPath.section].requests[requestIndexPath.row].blockedUsers.insert(blockedHelper)
        
        view?.set(requestSections: requestSections)
    }
    
    func didAcceptHelper(requestIndexPath: IndexPath, helperIndexPath: IndexPath) {
        // Remove and extract helper from potential helpers
        let helper = requestSections[requestIndexPath.section]
                    .requests[requestIndexPath.row]
                    .potentialHelpers.remove(at: helperIndexPath.row)
        
        // Set helper
        requestSections[requestIndexPath.section].requests[requestIndexPath.row].helper = helper
        
        view?.set(requestSections: requestSections)
    }
}
