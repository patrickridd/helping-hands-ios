//
//  MyRequestsProtocols.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/14/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

protocol MyRequestsPresenterLogic {
    
    var view: MyRequestsViewDelegate? { get set }
    init(view: MyRequestsViewDelegate)
    func newRequestButtonTapped()
    func getRequestSections()
    func modifyButtonTapped(at indexPath: IndexPath)
    func cancelButtonTapped(at indexPath: IndexPath)
    func removeRequest(at indexPath: IndexPath)
    func didAcceptHelper(requestIndexPath: IndexPath, helperIndexPath: IndexPath)
    func didBlockHelper(requestIndexPath: IndexPath, helperIndexPath: IndexPath)
    func minusHelperButtonTapped(at indexPath: IndexPath)
}

protocol MyRequestsViewDelegate {
    var presenter: MyRequestsPresenterLogic! { get set }
    func newRequestButtonTapped()
    func present(view: UIViewController)
    func push(view: UIViewController)
    func presentCreateRequestView(with request: Request?)
    func presentDeleteRequestAlert(for indexPath: IndexPath) 
    func set(requestSections: [RequestSection])
    func modifyButtonTapped(sender: ActionButton)
    func cancelButtonTapped(sender: ActionButton)
    func minusHelperButtonTapped(sender: ActionButton)
}

protocol HelperActionsLogic: class {
    func didAcceptHelper(requestIndexPath: IndexPath, helperIndexPath: IndexPath)
    func didBlockHelper(requestIndexPath: IndexPath, helperIndexPath: IndexPath)
}
