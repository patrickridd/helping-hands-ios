//
//  MyRequestsViewController.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/14/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

class MyRequestsViewController: UIViewController {
    
    @IBOutlet weak var newRequestButton: UIButton!
    @IBOutlet weak var doNotDisturbButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noOpenRequestsView: UIView!
    
    var presenter: MyRequestsPresenterLogic!
    
    var myRequestSections: [RequestSection] = [] {
        didSet {
            tableView.reloadData()
        }
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        
        presenter = MyRequestsPresenter(view: self)
        presenter.getRequestSections()
        newRequestButton.addTarget(self, action: #selector(newRequestButtonTapped), for: .touchUpInside)
    }
    
}

extension MyRequestsViewController: MyRequestsViewDelegate {
    
    @objc func newRequestButtonTapped() {
        presenter.newRequestButtonTapped()
    }
    
    func present(view: UIViewController) {
        present(view, animated: true, completion: nil)
    }
    
    func presentCreateRequestView(with request: Request? = nil) {
        let storyboard = UIStoryboard(name: "CreateRequest", bundle: nil)
        if let createRequestView = storyboard.instantiateViewController(identifier: "CreateRequestView") as? CreateRequestViewController {
            let navController = UINavigationController(rootViewController: createRequestView)
            createRequestView.request = request
            present(navController, animated: true, completion: nil)
        }
    }
    
    func presentDeleteRequestAlert(for indexPath: IndexPath) {
        let confirmAlertController = UIAlertController(title: "Delete Request?",
                                                       message: "Are you sure you want to delete this request?",
                                                       preferredStyle: .alert)
        let noAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        let yesAction = UIAlertAction(title: "Yes", style: .default) { [weak self] (_) in
            self?.presenter.removeRequest(at: indexPath)
        }
        
        confirmAlertController.addAction(yesAction)
        confirmAlertController.addAction(noAction)
        present(confirmAlertController, animated: true, completion: nil)
    }
    
    func push(view: UIViewController) {
        navigationController?.pushViewController(view, animated: true)
    }
    
    func set(requestSections: [RequestSection]) {
        myRequestSections = requestSections
        noOpenRequestsView.isHidden = myRequestSections.count != 0
    }
    
    @objc func modifyButtonTapped(sender: ActionButton) {
        if let indexPath = sender.indexPath {
            presenter.modifyButtonTapped(at: indexPath)
        }
    }
    
    @objc func cancelButtonTapped(sender: ActionButton) {
        if let indexPath = sender.indexPath {
            presenter.cancelButtonTapped(at: indexPath)
        }
    }
    
    @objc func minusHelperButtonTapped(sender: ActionButton) {
        if let requestIndexPath = sender.indexPath {
            presenter.minusHelperButtonTapped(at: requestIndexPath)
        }
    }
}

extension MyRequestsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return myRequestSections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let requestSection = myRequestSections[section]
        return requestSection.requests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myRequestCell = tableView.dequeueReusableCell(withIdentifier: "MyRequestCell",
                                                          for: indexPath) as? MyRequestTableViewCell
        let requestSection = myRequestSections[indexPath.section]
        let request = requestSection.requests[indexPath.row]
        myRequestCell?.update(with: request)
        myRequestCell?.set(indexPath: indexPath)
        myRequestCell?.modifyButton.addTarget(self, action: #selector(modifyButtonTapped(sender:)), for: .touchUpInside)
        myRequestCell?.cancelButton.addTarget(self, action: #selector(cancelButtonTapped(sender:)), for: .touchUpInside)
        myRequestCell?.minusHelperButton.addTarget(self,
                                                   action: #selector(minusHelperButtonTapped(sender:)),
                                                   for: .touchUpInside)
        myRequestCell?.requestIndexPath = indexPath
        myRequestCell?.delegate = self
        
        return myRequestCell ?? UITableViewCell()
    }
}

extension MyRequestsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let requestSection = myRequestSections[indexPath.section]
        let request = requestSection.requests[indexPath.row]
        let helperCellHeight = request.helper == nil ? CGFloat(request.potentialHelpers.count) * 60.0 : 0.0
        
        return 120 + helperCellHeight
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let requestSection = myRequestSections[section]
        return requestSection.requestDate.formatDateForDisplay()
    }
    
}

extension MyRequestsViewController: HelperActionsLogic {
  
    func didAcceptHelper(requestIndexPath: IndexPath, helperIndexPath: IndexPath) {
        presenter.didAcceptHelper(requestIndexPath: requestIndexPath, helperIndexPath: helperIndexPath)
    }
    
    func didBlockHelper(requestIndexPath: IndexPath, helperIndexPath: IndexPath) {
        presenter.didBlockHelper(requestIndexPath: requestIndexPath, helperIndexPath: helperIndexPath)
    }
}
