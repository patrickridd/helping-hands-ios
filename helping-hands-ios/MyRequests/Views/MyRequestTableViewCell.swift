//
//  MyRequestTableViewCell.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/18/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

class MyRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var requestTitleLabel: UILabel!
    @IBOutlet weak var chooseHelperLabel: UILabel!
    @IBOutlet weak var acceptedHelperStack: UIStackView!
    @IBOutlet weak var acceptedHelperLabel: UILabel!
    @IBOutlet weak var modifyButton: ActionButton!
    @IBOutlet weak var cancelButton: ActionButton!
    @IBOutlet weak var minusHelperButton: ActionButton!
    
    var request: Request?
    
    weak var delegate: HelperActionsLogic?
    var requestIndexPath: IndexPath?
    
    var potentialHelpers: [User] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        tableView.dataSource = self
        tableView.delegate = self
        
        let helperCell = UINib(nibName: "HelperCell", bundle: nil)
        tableView.register(helperCell, forCellReuseIdentifier: "HelperCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func update(with request: Request) {
        requestTitleLabel.text = request.title
        acceptedHelperStack.isHidden = request.helper == nil
        acceptedHelperLabel.text = request.helper?.fullName
        
        chooseHelperLabel.isHidden = request.helper != nil
        chooseHelperLabel.text = request.potentialHelpers.isEmpty ? "Waiting on potential helpers..." : "Choose Helper"
        potentialHelpers = (request.helper == nil) ? request.potentialHelpers : []
        self.request = request
    }
    
    func set(indexPath: IndexPath) {
        modifyButton.indexPath = indexPath
        cancelButton.indexPath = indexPath
        minusHelperButton.indexPath = indexPath
    }
    
    @objc func acceptButtonTapped(sender: ActionButton) {
        if let requestIndexPath = requestIndexPath, let helperIndexPath = sender.indexPath {
            delegate?.didAcceptHelper(requestIndexPath: requestIndexPath, helperIndexPath: helperIndexPath)
        }
    }
    
    @objc func blockButtonTapped(sender: ActionButton) {
        if let requestIndexPath = requestIndexPath, let helperIndexPath = sender.indexPath {
            delegate?.didBlockHelper(requestIndexPath: requestIndexPath, helperIndexPath: helperIndexPath)
        }
    }
}

extension MyRequestTableViewCell: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return potentialHelpers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let helperCell = tableView.dequeueReusableCell(withIdentifier: "HelperCell", for: indexPath) as? HelperTableViewCell
        
        let helper = potentialHelpers[indexPath.row]
   
        helperCell?.update(with: helper)
        helperCell?.acceptButton.addTarget(self, action: #selector(acceptButtonTapped(sender:)), for: .touchUpInside)
        helperCell?.blockButton.addTarget(self, action: #selector(blockButtonTapped(sender:)), for: .touchUpInside)
        helperCell?.set(indexPath: indexPath)
        
        return helperCell ?? UITableViewCell()
    }

}

extension MyRequestTableViewCell: UITableViewDelegate {

    
}
