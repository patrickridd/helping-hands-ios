//
//  AboutMeViewController.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/6/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

class AboutMeViewController: UIViewController, AboutMeViewDelegate {
   
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var updateButton: UIButton!
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var aboutMeTextView: UITextView!
    
    @IBOutlet weak var buttonContainerTopConstraint: NSLayoutConstraint!
    
    var presenter: AboutMePresenterLogic!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter = AboutMePresenter(view: self)
        userImageView.contentMode = .scaleAspectFill
        aboutMeTextView.inputAccessoryView = KeyboardToolbar(textView: aboutMeTextView)
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        skipButton.addTarget(self, action: #selector(skipButtonTapped), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
        updateButton.addTarget(self, action: #selector(updateButtonTapped), for: .touchUpInside)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        presenter.updateView()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

}

extension AboutMeViewController {
    
    @objc func backButtonTapped() {
        presenter.backButtonTapped()
    }
    
    @objc func skipButtonTapped() {
        presenter.skipButtonTapped()
    }
    
    @objc func nextButtonTapped() {
        presenter.nextButtonTapped(with: userImageView.image?.toUrl(), and: aboutMeTextView.text)
    }
    
    @objc func updateButtonTapped() {
        presenter.updateButtonTapped()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        UIView.animate(withDuration: 0.1, animations: { [weak self] () -> Void in
            self?.buttonContainerTopConstraint.constant = -80
        })
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.buttonContainerTopConstraint.constant = 0
    }
    
    func setUser(image: UIImage) {
        userImageView.image = image
    }
    
    func updateView(with user: User) {
        aboutMeTextView.text = user.description
        if let profileImage = user.imageUrl?.urlToImage() {
            setUser(image: profileImage)
        }
    }
    
    func presentImageAlertController() {
        let alertController = UIAlertController(title: "Profile Picture",
                                                message: nil,
                                                preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        let photoAction = UIAlertAction(title: "Photo Library", style: .default) { [weak self] (_) in
            self?.presenter.showPhotoLibrary()
        }
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { [weak self] (_) in
            self?.presenter.showCamera()
        }
        
        alertController.addAction(cameraAction)
        alertController.addAction(photoAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func navigateToAddressView() {
        let addressView = AddressViewController(nibName: "Address", bundle: nil)
        self.navigationController?.pushViewController(addressView, animated: true)
    }

    func popView() {
        navigationController?.popViewController(animated: true)
    }
    
    func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
