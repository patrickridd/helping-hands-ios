//
//  AboutMeProtocols.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/6/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

protocol AboutMeViewDelegate: class {
    var presenter: AboutMePresenterLogic! { get set }
    
    func backButtonTapped()
    func skipButtonTapped()
    func nextButtonTapped()
    func updateButtonTapped()
    func setUser(image: UIImage)
    func navigateToAddressView()
    func presentImageAlertController()
    func popView()
    func dismissView()
    func updateView(with user: User)

}

protocol AboutMePresenterLogic {
    var view: (AboutMeViewDelegate & UIViewController)? { get set }
    
     init(view: (AboutMeViewDelegate & UIViewController)?)
    
    func backButtonTapped()
    func skipButtonTapped()
    func nextButtonTapped(with imageUrl: String?, and description: String)
    func updateButtonTapped()
    func showCamera()
    func showPhotoLibrary()
    func updateView()
}
