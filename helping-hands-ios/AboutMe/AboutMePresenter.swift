//
//  AboutMePresenter.swift
//  helping-hands-ios
//
//  Created by Patrick Ridd (patrick.ridd@stgconsulting.com) on 11/6/19.
//  Copyright © 2019 patrickridd. All rights reserved.
//

import UIKit

class AboutMePresenter: AboutMePresenterLogic {

    weak var view: (AboutMeViewDelegate & UIViewController)?
    
    let photoManager: PhotoServiceManagerLogic = PhotoServiceManager()
    let userDefaults: UserDefaultsDelegate = UserDefaultsService()
    let appSyncService: AppSyncServiceDelegate = AppSyncService()
    
    required init(view: (AboutMeViewDelegate & UIViewController)?) {
        self.view = view
    }
    
    func updateView() {
        if let user = userDefaults.getUser() {
            view?.updateView(with: user)
        }
    }
    
    func backButtonTapped() {
        view?.popView()
    }
    
    func skipButtonTapped() {
        view?.dismissView()
    }
    
    func nextButtonTapped(with imageUrl: String?, and description: String) {
        if let user = userDefaults.getUser() {
            user.imageUrl = imageUrl
            user.description = description
            userDefaults.save(user: user)
            appSyncService.update(user: user) { (error) in
                print(error)
            }
        }
        view?.navigateToAddressView()
    }
    
    func updateButtonTapped() {
        view?.presentImageAlertController()
    }
    
    func showCamera() {
        photoManager.showCamera(on: view!, using: self)
    }
    
    func showPhotoLibrary() {
        photoManager.showPhotoLibrary(on: view!, using: self)
    }
    
}

extension AboutMePresenter: PhotoReceiver {
   
    func didReceive(image: UIImage) {
        view?.setUser(image: image)
    }
}

