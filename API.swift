//  This file was automatically generated and should not be edited.

import AWSAppSync

public struct CreateUserInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID? = nil, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil) {
    graphQLMap = ["id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "helperRatings": helperRatings, "imageUrl": imageUrl]
  }

  public var id: GraphQLID? {
    get {
      return graphQLMap["id"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var email: String {
    get {
      return graphQLMap["email"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "email")
    }
  }

  public var phoneNumber: String {
    get {
      return graphQLMap["phoneNumber"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "phoneNumber")
    }
  }

  public var firstName: String? {
    get {
      return graphQLMap["firstName"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "firstName")
    }
  }

  public var lastName: String? {
    get {
      return graphQLMap["lastName"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lastName")
    }
  }

  public var preferredContact: [PreferredContact?]? {
    get {
      return graphQLMap["preferredContact"] as! [PreferredContact?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "preferredContact")
    }
  }

  public var description: String? {
    get {
      return graphQLMap["description"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "description")
    }
  }

  public var helperRatings: [Double?]? {
    get {
      return graphQLMap["helperRatings"] as! [Double?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "helperRatings")
    }
  }

  public var imageUrl: String? {
    get {
      return graphQLMap["imageUrl"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "imageUrl")
    }
  }
}

public enum PreferredContact: RawRepresentable, Equatable, JSONDecodable, JSONEncodable {
  public typealias RawValue = String
  case call
  case text
  case email
  /// Auto generated constant for unknown enum values
  case unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
      case "CALL": self = .call
      case "TEXT": self = .text
      case "EMAIL": self = .email
      default: self = .unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
      case .call: return "CALL"
      case .text: return "TEXT"
      case .email: return "EMAIL"
      case .unknown(let value): return value
    }
  }

  public static func == (lhs: PreferredContact, rhs: PreferredContact) -> Bool {
    switch (lhs, rhs) {
      case (.call, .call): return true
      case (.text, .text): return true
      case (.email, .email): return true
      case (.unknown(let lhsValue), .unknown(let rhsValue)): return lhsValue == rhsValue
      default: return false
    }
  }
}

public struct UpdateUserInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID, email: String? = nil, phoneNumber: String? = nil, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil) {
    graphQLMap = ["id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "helperRatings": helperRatings, "imageUrl": imageUrl]
  }

  public var id: GraphQLID {
    get {
      return graphQLMap["id"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var email: String? {
    get {
      return graphQLMap["email"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "email")
    }
  }

  public var phoneNumber: String? {
    get {
      return graphQLMap["phoneNumber"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "phoneNumber")
    }
  }

  public var firstName: String? {
    get {
      return graphQLMap["firstName"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "firstName")
    }
  }

  public var lastName: String? {
    get {
      return graphQLMap["lastName"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lastName")
    }
  }

  public var preferredContact: [PreferredContact?]? {
    get {
      return graphQLMap["preferredContact"] as! [PreferredContact?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "preferredContact")
    }
  }

  public var description: String? {
    get {
      return graphQLMap["description"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "description")
    }
  }

  public var helperRatings: [Double?]? {
    get {
      return graphQLMap["helperRatings"] as! [Double?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "helperRatings")
    }
  }

  public var imageUrl: String? {
    get {
      return graphQLMap["imageUrl"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "imageUrl")
    }
  }
}

public struct DeleteUserInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID? = nil) {
    graphQLMap = ["id": id]
  }

  public var id: GraphQLID? {
    get {
      return graphQLMap["id"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }
}

public struct CreateRequestInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID? = nil, title: String, description: String, duration: String, latitude: Double, longitude: Double, requestRequesterId: GraphQLID) {
    graphQLMap = ["id": id, "title": title, "description": description, "duration": duration, "latitude": latitude, "longitude": longitude, "requestRequesterId": requestRequesterId]
  }

  public var id: GraphQLID? {
    get {
      return graphQLMap["id"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var title: String {
    get {
      return graphQLMap["title"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "title")
    }
  }

  public var description: String {
    get {
      return graphQLMap["description"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "description")
    }
  }

  public var duration: String {
    get {
      return graphQLMap["duration"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "duration")
    }
  }

  public var latitude: Double {
    get {
      return graphQLMap["latitude"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "latitude")
    }
  }

  public var longitude: Double {
    get {
      return graphQLMap["longitude"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "longitude")
    }
  }

  public var requestRequesterId: GraphQLID {
    get {
      return graphQLMap["requestRequesterId"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "requestRequesterId")
    }
  }
}

public struct UpdateRequestInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID, title: String? = nil, description: String? = nil, duration: String? = nil, latitude: Double? = nil, longitude: Double? = nil, requestRequesterId: GraphQLID? = nil) {
    graphQLMap = ["id": id, "title": title, "description": description, "duration": duration, "latitude": latitude, "longitude": longitude, "requestRequesterId": requestRequesterId]
  }

  public var id: GraphQLID {
    get {
      return graphQLMap["id"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var title: String? {
    get {
      return graphQLMap["title"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "title")
    }
  }

  public var description: String? {
    get {
      return graphQLMap["description"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "description")
    }
  }

  public var duration: String? {
    get {
      return graphQLMap["duration"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "duration")
    }
  }

  public var latitude: Double? {
    get {
      return graphQLMap["latitude"] as! Double?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "latitude")
    }
  }

  public var longitude: Double? {
    get {
      return graphQLMap["longitude"] as! Double?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "longitude")
    }
  }

  public var requestRequesterId: GraphQLID? {
    get {
      return graphQLMap["requestRequesterId"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "requestRequesterId")
    }
  }
}

public struct DeleteRequestInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID? = nil) {
    graphQLMap = ["id": id]
  }

  public var id: GraphQLID? {
    get {
      return graphQLMap["id"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }
}

public struct ModelUserFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: ModelIDFilterInput? = nil, email: ModelStringFilterInput? = nil, phoneNumber: ModelStringFilterInput? = nil, firstName: ModelStringFilterInput? = nil, lastName: ModelStringFilterInput? = nil, preferredContact: ModelPreferredContactListFilterInput? = nil, description: ModelStringFilterInput? = nil, helperRatings: ModelFloatFilterInput? = nil, imageUrl: ModelStringFilterInput? = nil, and: [ModelUserFilterInput?]? = nil, or: [ModelUserFilterInput?]? = nil, not: ModelUserFilterInput? = nil) {
    graphQLMap = ["id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "helperRatings": helperRatings, "imageUrl": imageUrl, "and": and, "or": or, "not": not]
  }

  public var id: ModelIDFilterInput? {
    get {
      return graphQLMap["id"] as! ModelIDFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var email: ModelStringFilterInput? {
    get {
      return graphQLMap["email"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "email")
    }
  }

  public var phoneNumber: ModelStringFilterInput? {
    get {
      return graphQLMap["phoneNumber"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "phoneNumber")
    }
  }

  public var firstName: ModelStringFilterInput? {
    get {
      return graphQLMap["firstName"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "firstName")
    }
  }

  public var lastName: ModelStringFilterInput? {
    get {
      return graphQLMap["lastName"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lastName")
    }
  }

  public var preferredContact: ModelPreferredContactListFilterInput? {
    get {
      return graphQLMap["preferredContact"] as! ModelPreferredContactListFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "preferredContact")
    }
  }

  public var description: ModelStringFilterInput? {
    get {
      return graphQLMap["description"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "description")
    }
  }

  public var helperRatings: ModelFloatFilterInput? {
    get {
      return graphQLMap["helperRatings"] as! ModelFloatFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "helperRatings")
    }
  }

  public var imageUrl: ModelStringFilterInput? {
    get {
      return graphQLMap["imageUrl"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "imageUrl")
    }
  }

  public var and: [ModelUserFilterInput?]? {
    get {
      return graphQLMap["and"] as! [ModelUserFilterInput?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "and")
    }
  }

  public var or: [ModelUserFilterInput?]? {
    get {
      return graphQLMap["or"] as! [ModelUserFilterInput?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "or")
    }
  }

  public var not: ModelUserFilterInput? {
    get {
      return graphQLMap["not"] as! ModelUserFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "not")
    }
  }
}

public struct ModelIDFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(ne: GraphQLID? = nil, eq: GraphQLID? = nil, le: GraphQLID? = nil, lt: GraphQLID? = nil, ge: GraphQLID? = nil, gt: GraphQLID? = nil, contains: GraphQLID? = nil, notContains: GraphQLID? = nil, between: [GraphQLID?]? = nil, beginsWith: GraphQLID? = nil) {
    graphQLMap = ["ne": ne, "eq": eq, "le": le, "lt": lt, "ge": ge, "gt": gt, "contains": contains, "notContains": notContains, "between": between, "beginsWith": beginsWith]
  }

  public var ne: GraphQLID? {
    get {
      return graphQLMap["ne"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ne")
    }
  }

  public var eq: GraphQLID? {
    get {
      return graphQLMap["eq"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eq")
    }
  }

  public var le: GraphQLID? {
    get {
      return graphQLMap["le"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "le")
    }
  }

  public var lt: GraphQLID? {
    get {
      return graphQLMap["lt"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lt")
    }
  }

  public var ge: GraphQLID? {
    get {
      return graphQLMap["ge"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ge")
    }
  }

  public var gt: GraphQLID? {
    get {
      return graphQLMap["gt"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gt")
    }
  }

  public var contains: GraphQLID? {
    get {
      return graphQLMap["contains"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "contains")
    }
  }

  public var notContains: GraphQLID? {
    get {
      return graphQLMap["notContains"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "notContains")
    }
  }

  public var between: [GraphQLID?]? {
    get {
      return graphQLMap["between"] as! [GraphQLID?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "between")
    }
  }

  public var beginsWith: GraphQLID? {
    get {
      return graphQLMap["beginsWith"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "beginsWith")
    }
  }
}

public struct ModelStringFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(ne: String? = nil, eq: String? = nil, le: String? = nil, lt: String? = nil, ge: String? = nil, gt: String? = nil, contains: String? = nil, notContains: String? = nil, between: [String?]? = nil, beginsWith: String? = nil) {
    graphQLMap = ["ne": ne, "eq": eq, "le": le, "lt": lt, "ge": ge, "gt": gt, "contains": contains, "notContains": notContains, "between": between, "beginsWith": beginsWith]
  }

  public var ne: String? {
    get {
      return graphQLMap["ne"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ne")
    }
  }

  public var eq: String? {
    get {
      return graphQLMap["eq"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eq")
    }
  }

  public var le: String? {
    get {
      return graphQLMap["le"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "le")
    }
  }

  public var lt: String? {
    get {
      return graphQLMap["lt"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lt")
    }
  }

  public var ge: String? {
    get {
      return graphQLMap["ge"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ge")
    }
  }

  public var gt: String? {
    get {
      return graphQLMap["gt"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gt")
    }
  }

  public var contains: String? {
    get {
      return graphQLMap["contains"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "contains")
    }
  }

  public var notContains: String? {
    get {
      return graphQLMap["notContains"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "notContains")
    }
  }

  public var between: [String?]? {
    get {
      return graphQLMap["between"] as! [String?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "between")
    }
  }

  public var beginsWith: String? {
    get {
      return graphQLMap["beginsWith"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "beginsWith")
    }
  }
}

public struct ModelPreferredContactListFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(eq: [PreferredContact?]? = nil, ne: [PreferredContact?]? = nil, contains: PreferredContact? = nil, notContains: PreferredContact? = nil) {
    graphQLMap = ["eq": eq, "ne": ne, "contains": contains, "notContains": notContains]
  }

  public var eq: [PreferredContact?]? {
    get {
      return graphQLMap["eq"] as! [PreferredContact?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eq")
    }
  }

  public var ne: [PreferredContact?]? {
    get {
      return graphQLMap["ne"] as! [PreferredContact?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ne")
    }
  }

  public var contains: PreferredContact? {
    get {
      return graphQLMap["contains"] as! PreferredContact?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "contains")
    }
  }

  public var notContains: PreferredContact? {
    get {
      return graphQLMap["notContains"] as! PreferredContact?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "notContains")
    }
  }
}

public struct ModelFloatFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(ne: Double? = nil, eq: Double? = nil, le: Double? = nil, lt: Double? = nil, ge: Double? = nil, gt: Double? = nil, contains: Double? = nil, notContains: Double? = nil, between: [Double?]? = nil) {
    graphQLMap = ["ne": ne, "eq": eq, "le": le, "lt": lt, "ge": ge, "gt": gt, "contains": contains, "notContains": notContains, "between": between]
  }

  public var ne: Double? {
    get {
      return graphQLMap["ne"] as! Double?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ne")
    }
  }

  public var eq: Double? {
    get {
      return graphQLMap["eq"] as! Double?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eq")
    }
  }

  public var le: Double? {
    get {
      return graphQLMap["le"] as! Double?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "le")
    }
  }

  public var lt: Double? {
    get {
      return graphQLMap["lt"] as! Double?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lt")
    }
  }

  public var ge: Double? {
    get {
      return graphQLMap["ge"] as! Double?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ge")
    }
  }

  public var gt: Double? {
    get {
      return graphQLMap["gt"] as! Double?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gt")
    }
  }

  public var contains: Double? {
    get {
      return graphQLMap["contains"] as! Double?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "contains")
    }
  }

  public var notContains: Double? {
    get {
      return graphQLMap["notContains"] as! Double?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "notContains")
    }
  }

  public var between: [Double?]? {
    get {
      return graphQLMap["between"] as! [Double?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "between")
    }
  }
}

public struct ModelRequestFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: ModelIDFilterInput? = nil, title: ModelStringFilterInput? = nil, description: ModelStringFilterInput? = nil, duration: ModelStringFilterInput? = nil, latitude: ModelFloatFilterInput? = nil, longitude: ModelFloatFilterInput? = nil, and: [ModelRequestFilterInput?]? = nil, or: [ModelRequestFilterInput?]? = nil, not: ModelRequestFilterInput? = nil) {
    graphQLMap = ["id": id, "title": title, "description": description, "duration": duration, "latitude": latitude, "longitude": longitude, "and": and, "or": or, "not": not]
  }

  public var id: ModelIDFilterInput? {
    get {
      return graphQLMap["id"] as! ModelIDFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var title: ModelStringFilterInput? {
    get {
      return graphQLMap["title"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "title")
    }
  }

  public var description: ModelStringFilterInput? {
    get {
      return graphQLMap["description"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "description")
    }
  }

  public var duration: ModelStringFilterInput? {
    get {
      return graphQLMap["duration"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "duration")
    }
  }

  public var latitude: ModelFloatFilterInput? {
    get {
      return graphQLMap["latitude"] as! ModelFloatFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "latitude")
    }
  }

  public var longitude: ModelFloatFilterInput? {
    get {
      return graphQLMap["longitude"] as! ModelFloatFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "longitude")
    }
  }

  public var and: [ModelRequestFilterInput?]? {
    get {
      return graphQLMap["and"] as! [ModelRequestFilterInput?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "and")
    }
  }

  public var or: [ModelRequestFilterInput?]? {
    get {
      return graphQLMap["or"] as! [ModelRequestFilterInput?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "or")
    }
  }

  public var not: ModelRequestFilterInput? {
    get {
      return graphQLMap["not"] as! ModelRequestFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "not")
    }
  }
}

public final class CreateUserMutation: GraphQLMutation {
  public static let operationString =
    "mutation CreateUser($input: CreateUserInput!) {\n  createUser(input: $input) {\n    __typename\n    id\n    email\n    phoneNumber\n    firstName\n    lastName\n    preferredContact\n    description\n    requests {\n      __typename\n      items {\n        __typename\n        id\n        title\n        description\n        duration\n        latitude\n        longitude\n        owner\n      }\n      nextToken\n    }\n    helperRatings\n    imageUrl\n    owner\n  }\n}"

  public var input: CreateUserInput

  public init(input: CreateUserInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createUser", arguments: ["input": GraphQLVariable("input")], type: .object(CreateUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(createUser: CreateUser? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "createUser": createUser.flatMap { $0.snapshot }])
    }

    public var createUser: CreateUser? {
      get {
        return (snapshot["createUser"] as? Snapshot).flatMap { CreateUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "createUser")
      }
    }

    public struct CreateUser: GraphQLSelectionSet {
      public static let possibleTypes = ["User"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("email", type: .nonNull(.scalar(String.self))),
        GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
        GraphQLField("firstName", type: .scalar(String.self)),
        GraphQLField("lastName", type: .scalar(String.self)),
        GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
        GraphQLField("description", type: .scalar(String.self)),
        GraphQLField("requests", type: .object(Request.selections)),
        GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
        GraphQLField("imageUrl", type: .scalar(String.self)),
        GraphQLField("owner", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
        self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var email: String {
        get {
          return snapshot["email"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "email")
        }
      }

      public var phoneNumber: String {
        get {
          return snapshot["phoneNumber"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "phoneNumber")
        }
      }

      public var firstName: String? {
        get {
          return snapshot["firstName"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "firstName")
        }
      }

      public var lastName: String? {
        get {
          return snapshot["lastName"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "lastName")
        }
      }

      public var preferredContact: [PreferredContact?]? {
        get {
          return snapshot["preferredContact"] as? [PreferredContact?]
        }
        set {
          snapshot.updateValue(newValue, forKey: "preferredContact")
        }
      }

      public var description: String? {
        get {
          return snapshot["description"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "description")
        }
      }

      public var requests: Request? {
        get {
          return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
        }
        set {
          snapshot.updateValue(newValue?.snapshot, forKey: "requests")
        }
      }

      public var helperRatings: [Double?]? {
        get {
          return snapshot["helperRatings"] as? [Double?]
        }
        set {
          snapshot.updateValue(newValue, forKey: "helperRatings")
        }
      }

      public var imageUrl: String? {
        get {
          return snapshot["imageUrl"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "imageUrl")
        }
      }

      public var owner: String? {
        get {
          return snapshot["owner"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "owner")
        }
      }

      public struct Request: GraphQLSelectionSet {
        public static let possibleTypes = ["ModelRequestConnection"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("items", type: .list(.object(Item.selections))),
          GraphQLField("nextToken", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(items: [Item?]? = nil, nextToken: String? = nil) {
          self.init(snapshot: ["__typename": "ModelRequestConnection", "items": items.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "nextToken": nextToken])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var items: [Item?]? {
          get {
            return (snapshot["items"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { Item(snapshot: $0) } } }
          }
          set {
            snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "items")
          }
        }

        public var nextToken: String? {
          get {
            return snapshot["nextToken"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "nextToken")
          }
        }

        public struct Item: GraphQLSelectionSet {
          public static let possibleTypes = ["Request"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
            GraphQLField("title", type: .nonNull(.scalar(String.self))),
            GraphQLField("description", type: .nonNull(.scalar(String.self))),
            GraphQLField("duration", type: .nonNull(.scalar(String.self))),
            GraphQLField("latitude", type: .nonNull(.scalar(Double.self))),
            GraphQLField("longitude", type: .nonNull(.scalar(Double.self))),
            GraphQLField("owner", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(id: GraphQLID, title: String, description: String, duration: String, latitude: Double, longitude: Double, owner: String? = nil) {
            self.init(snapshot: ["__typename": "Request", "id": id, "title": title, "description": description, "duration": duration, "latitude": latitude, "longitude": longitude, "owner": owner])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID {
            get {
              return snapshot["id"]! as! GraphQLID
            }
            set {
              snapshot.updateValue(newValue, forKey: "id")
            }
          }

          public var title: String {
            get {
              return snapshot["title"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "title")
            }
          }

          public var description: String {
            get {
              return snapshot["description"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "description")
            }
          }

          public var duration: String {
            get {
              return snapshot["duration"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "duration")
            }
          }

          public var latitude: Double {
            get {
              return snapshot["latitude"]! as! Double
            }
            set {
              snapshot.updateValue(newValue, forKey: "latitude")
            }
          }

          public var longitude: Double {
            get {
              return snapshot["longitude"]! as! Double
            }
            set {
              snapshot.updateValue(newValue, forKey: "longitude")
            }
          }

          public var owner: String? {
            get {
              return snapshot["owner"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "owner")
            }
          }
        }
      }
    }
  }
}

public final class UpdateUserMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateUser($input: UpdateUserInput!) {\n  updateUser(input: $input) {\n    __typename\n    id\n    email\n    phoneNumber\n    firstName\n    lastName\n    preferredContact\n    description\n    requests {\n      __typename\n      items {\n        __typename\n        id\n        title\n        description\n        duration\n        latitude\n        longitude\n        owner\n      }\n      nextToken\n    }\n    helperRatings\n    imageUrl\n    owner\n  }\n}"

  public var input: UpdateUserInput

  public init(input: UpdateUserInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateUser", arguments: ["input": GraphQLVariable("input")], type: .object(UpdateUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateUser: UpdateUser? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateUser": updateUser.flatMap { $0.snapshot }])
    }

    public var updateUser: UpdateUser? {
      get {
        return (snapshot["updateUser"] as? Snapshot).flatMap { UpdateUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateUser")
      }
    }

    public struct UpdateUser: GraphQLSelectionSet {
      public static let possibleTypes = ["User"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("email", type: .nonNull(.scalar(String.self))),
        GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
        GraphQLField("firstName", type: .scalar(String.self)),
        GraphQLField("lastName", type: .scalar(String.self)),
        GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
        GraphQLField("description", type: .scalar(String.self)),
        GraphQLField("requests", type: .object(Request.selections)),
        GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
        GraphQLField("imageUrl", type: .scalar(String.self)),
        GraphQLField("owner", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
        self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var email: String {
        get {
          return snapshot["email"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "email")
        }
      }

      public var phoneNumber: String {
        get {
          return snapshot["phoneNumber"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "phoneNumber")
        }
      }

      public var firstName: String? {
        get {
          return snapshot["firstName"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "firstName")
        }
      }

      public var lastName: String? {
        get {
          return snapshot["lastName"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "lastName")
        }
      }

      public var preferredContact: [PreferredContact?]? {
        get {
          return snapshot["preferredContact"] as? [PreferredContact?]
        }
        set {
          snapshot.updateValue(newValue, forKey: "preferredContact")
        }
      }

      public var description: String? {
        get {
          return snapshot["description"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "description")
        }
      }

      public var requests: Request? {
        get {
          return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
        }
        set {
          snapshot.updateValue(newValue?.snapshot, forKey: "requests")
        }
      }

      public var helperRatings: [Double?]? {
        get {
          return snapshot["helperRatings"] as? [Double?]
        }
        set {
          snapshot.updateValue(newValue, forKey: "helperRatings")
        }
      }

      public var imageUrl: String? {
        get {
          return snapshot["imageUrl"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "imageUrl")
        }
      }

      public var owner: String? {
        get {
          return snapshot["owner"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "owner")
        }
      }

      public struct Request: GraphQLSelectionSet {
        public static let possibleTypes = ["ModelRequestConnection"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("items", type: .list(.object(Item.selections))),
          GraphQLField("nextToken", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(items: [Item?]? = nil, nextToken: String? = nil) {
          self.init(snapshot: ["__typename": "ModelRequestConnection", "items": items.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "nextToken": nextToken])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var items: [Item?]? {
          get {
            return (snapshot["items"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { Item(snapshot: $0) } } }
          }
          set {
            snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "items")
          }
        }

        public var nextToken: String? {
          get {
            return snapshot["nextToken"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "nextToken")
          }
        }

        public struct Item: GraphQLSelectionSet {
          public static let possibleTypes = ["Request"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
            GraphQLField("title", type: .nonNull(.scalar(String.self))),
            GraphQLField("description", type: .nonNull(.scalar(String.self))),
            GraphQLField("duration", type: .nonNull(.scalar(String.self))),
            GraphQLField("latitude", type: .nonNull(.scalar(Double.self))),
            GraphQLField("longitude", type: .nonNull(.scalar(Double.self))),
            GraphQLField("owner", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(id: GraphQLID, title: String, description: String, duration: String, latitude: Double, longitude: Double, owner: String? = nil) {
            self.init(snapshot: ["__typename": "Request", "id": id, "title": title, "description": description, "duration": duration, "latitude": latitude, "longitude": longitude, "owner": owner])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID {
            get {
              return snapshot["id"]! as! GraphQLID
            }
            set {
              snapshot.updateValue(newValue, forKey: "id")
            }
          }

          public var title: String {
            get {
              return snapshot["title"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "title")
            }
          }

          public var description: String {
            get {
              return snapshot["description"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "description")
            }
          }

          public var duration: String {
            get {
              return snapshot["duration"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "duration")
            }
          }

          public var latitude: Double {
            get {
              return snapshot["latitude"]! as! Double
            }
            set {
              snapshot.updateValue(newValue, forKey: "latitude")
            }
          }

          public var longitude: Double {
            get {
              return snapshot["longitude"]! as! Double
            }
            set {
              snapshot.updateValue(newValue, forKey: "longitude")
            }
          }

          public var owner: String? {
            get {
              return snapshot["owner"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "owner")
            }
          }
        }
      }
    }
  }
}

public final class DeleteUserMutation: GraphQLMutation {
  public static let operationString =
    "mutation DeleteUser($input: DeleteUserInput!) {\n  deleteUser(input: $input) {\n    __typename\n    id\n    email\n    phoneNumber\n    firstName\n    lastName\n    preferredContact\n    description\n    requests {\n      __typename\n      items {\n        __typename\n        id\n        title\n        description\n        duration\n        latitude\n        longitude\n        owner\n      }\n      nextToken\n    }\n    helperRatings\n    imageUrl\n    owner\n  }\n}"

  public var input: DeleteUserInput

  public init(input: DeleteUserInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteUser", arguments: ["input": GraphQLVariable("input")], type: .object(DeleteUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(deleteUser: DeleteUser? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "deleteUser": deleteUser.flatMap { $0.snapshot }])
    }

    public var deleteUser: DeleteUser? {
      get {
        return (snapshot["deleteUser"] as? Snapshot).flatMap { DeleteUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "deleteUser")
      }
    }

    public struct DeleteUser: GraphQLSelectionSet {
      public static let possibleTypes = ["User"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("email", type: .nonNull(.scalar(String.self))),
        GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
        GraphQLField("firstName", type: .scalar(String.self)),
        GraphQLField("lastName", type: .scalar(String.self)),
        GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
        GraphQLField("description", type: .scalar(String.self)),
        GraphQLField("requests", type: .object(Request.selections)),
        GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
        GraphQLField("imageUrl", type: .scalar(String.self)),
        GraphQLField("owner", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
        self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var email: String {
        get {
          return snapshot["email"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "email")
        }
      }

      public var phoneNumber: String {
        get {
          return snapshot["phoneNumber"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "phoneNumber")
        }
      }

      public var firstName: String? {
        get {
          return snapshot["firstName"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "firstName")
        }
      }

      public var lastName: String? {
        get {
          return snapshot["lastName"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "lastName")
        }
      }

      public var preferredContact: [PreferredContact?]? {
        get {
          return snapshot["preferredContact"] as? [PreferredContact?]
        }
        set {
          snapshot.updateValue(newValue, forKey: "preferredContact")
        }
      }

      public var description: String? {
        get {
          return snapshot["description"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "description")
        }
      }

      public var requests: Request? {
        get {
          return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
        }
        set {
          snapshot.updateValue(newValue?.snapshot, forKey: "requests")
        }
      }

      public var helperRatings: [Double?]? {
        get {
          return snapshot["helperRatings"] as? [Double?]
        }
        set {
          snapshot.updateValue(newValue, forKey: "helperRatings")
        }
      }

      public var imageUrl: String? {
        get {
          return snapshot["imageUrl"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "imageUrl")
        }
      }

      public var owner: String? {
        get {
          return snapshot["owner"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "owner")
        }
      }

      public struct Request: GraphQLSelectionSet {
        public static let possibleTypes = ["ModelRequestConnection"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("items", type: .list(.object(Item.selections))),
          GraphQLField("nextToken", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(items: [Item?]? = nil, nextToken: String? = nil) {
          self.init(snapshot: ["__typename": "ModelRequestConnection", "items": items.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "nextToken": nextToken])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var items: [Item?]? {
          get {
            return (snapshot["items"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { Item(snapshot: $0) } } }
          }
          set {
            snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "items")
          }
        }

        public var nextToken: String? {
          get {
            return snapshot["nextToken"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "nextToken")
          }
        }

        public struct Item: GraphQLSelectionSet {
          public static let possibleTypes = ["Request"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
            GraphQLField("title", type: .nonNull(.scalar(String.self))),
            GraphQLField("description", type: .nonNull(.scalar(String.self))),
            GraphQLField("duration", type: .nonNull(.scalar(String.self))),
            GraphQLField("latitude", type: .nonNull(.scalar(Double.self))),
            GraphQLField("longitude", type: .nonNull(.scalar(Double.self))),
            GraphQLField("owner", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(id: GraphQLID, title: String, description: String, duration: String, latitude: Double, longitude: Double, owner: String? = nil) {
            self.init(snapshot: ["__typename": "Request", "id": id, "title": title, "description": description, "duration": duration, "latitude": latitude, "longitude": longitude, "owner": owner])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID {
            get {
              return snapshot["id"]! as! GraphQLID
            }
            set {
              snapshot.updateValue(newValue, forKey: "id")
            }
          }

          public var title: String {
            get {
              return snapshot["title"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "title")
            }
          }

          public var description: String {
            get {
              return snapshot["description"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "description")
            }
          }

          public var duration: String {
            get {
              return snapshot["duration"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "duration")
            }
          }

          public var latitude: Double {
            get {
              return snapshot["latitude"]! as! Double
            }
            set {
              snapshot.updateValue(newValue, forKey: "latitude")
            }
          }

          public var longitude: Double {
            get {
              return snapshot["longitude"]! as! Double
            }
            set {
              snapshot.updateValue(newValue, forKey: "longitude")
            }
          }

          public var owner: String? {
            get {
              return snapshot["owner"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "owner")
            }
          }
        }
      }
    }
  }
}

public final class CreateRequestMutation: GraphQLMutation {
  public static let operationString =
    "mutation CreateRequest($input: CreateRequestInput!) {\n  createRequest(input: $input) {\n    __typename\n    id\n    title\n    requester {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    description\n    duration\n    latitude\n    longitude\n    potentialHelpers {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    helper {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    blockedUsers {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    owner\n  }\n}"

  public var input: CreateRequestInput

  public init(input: CreateRequestInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createRequest", arguments: ["input": GraphQLVariable("input")], type: .object(CreateRequest.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(createRequest: CreateRequest? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "createRequest": createRequest.flatMap { $0.snapshot }])
    }

    public var createRequest: CreateRequest? {
      get {
        return (snapshot["createRequest"] as? Snapshot).flatMap { CreateRequest(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "createRequest")
      }
    }

    public struct CreateRequest: GraphQLSelectionSet {
      public static let possibleTypes = ["Request"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("title", type: .nonNull(.scalar(String.self))),
        GraphQLField("requester", type: .nonNull(.object(Requester.selections))),
        GraphQLField("description", type: .nonNull(.scalar(String.self))),
        GraphQLField("duration", type: .nonNull(.scalar(String.self))),
        GraphQLField("latitude", type: .nonNull(.scalar(Double.self))),
        GraphQLField("longitude", type: .nonNull(.scalar(Double.self))),
        GraphQLField("potentialHelpers", type: .list(.object(PotentialHelper.selections))),
        GraphQLField("helper", type: .object(Helper.selections)),
        GraphQLField("blockedUsers", type: .list(.object(BlockedUser.selections))),
        GraphQLField("owner", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, title: String, requester: Requester, description: String, duration: String, latitude: Double, longitude: Double, potentialHelpers: [PotentialHelper?]? = nil, helper: Helper? = nil, blockedUsers: [BlockedUser?]? = nil, owner: String? = nil) {
        self.init(snapshot: ["__typename": "Request", "id": id, "title": title, "requester": requester.snapshot, "description": description, "duration": duration, "latitude": latitude, "longitude": longitude, "potentialHelpers": potentialHelpers.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "helper": helper.flatMap { $0.snapshot }, "blockedUsers": blockedUsers.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "owner": owner])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var title: String {
        get {
          return snapshot["title"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "title")
        }
      }

      public var requester: Requester {
        get {
          return Requester(snapshot: snapshot["requester"]! as! Snapshot)
        }
        set {
          snapshot.updateValue(newValue.snapshot, forKey: "requester")
        }
      }

      public var description: String {
        get {
          return snapshot["description"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "description")
        }
      }

      public var duration: String {
        get {
          return snapshot["duration"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "duration")
        }
      }

      public var latitude: Double {
        get {
          return snapshot["latitude"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "latitude")
        }
      }

      public var longitude: Double {
        get {
          return snapshot["longitude"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "longitude")
        }
      }

      public var potentialHelpers: [PotentialHelper?]? {
        get {
          return (snapshot["potentialHelpers"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { PotentialHelper(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "potentialHelpers")
        }
      }

      public var helper: Helper? {
        get {
          return (snapshot["helper"] as? Snapshot).flatMap { Helper(snapshot: $0) }
        }
        set {
          snapshot.updateValue(newValue?.snapshot, forKey: "helper")
        }
      }

      public var blockedUsers: [BlockedUser?]? {
        get {
          return (snapshot["blockedUsers"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { BlockedUser(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "blockedUsers")
        }
      }

      public var owner: String? {
        get {
          return snapshot["owner"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "owner")
        }
      }

      public struct Requester: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct PotentialHelper: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct Helper: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct BlockedUser: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }
    }
  }
}

public final class UpdateRequestMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateRequest($input: UpdateRequestInput!) {\n  updateRequest(input: $input) {\n    __typename\n    id\n    title\n    requester {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    description\n    duration\n    latitude\n    longitude\n    potentialHelpers {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    helper {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    blockedUsers {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    owner\n  }\n}"

  public var input: UpdateRequestInput

  public init(input: UpdateRequestInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateRequest", arguments: ["input": GraphQLVariable("input")], type: .object(UpdateRequest.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateRequest: UpdateRequest? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateRequest": updateRequest.flatMap { $0.snapshot }])
    }

    public var updateRequest: UpdateRequest? {
      get {
        return (snapshot["updateRequest"] as? Snapshot).flatMap { UpdateRequest(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateRequest")
      }
    }

    public struct UpdateRequest: GraphQLSelectionSet {
      public static let possibleTypes = ["Request"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("title", type: .nonNull(.scalar(String.self))),
        GraphQLField("requester", type: .nonNull(.object(Requester.selections))),
        GraphQLField("description", type: .nonNull(.scalar(String.self))),
        GraphQLField("duration", type: .nonNull(.scalar(String.self))),
        GraphQLField("latitude", type: .nonNull(.scalar(Double.self))),
        GraphQLField("longitude", type: .nonNull(.scalar(Double.self))),
        GraphQLField("potentialHelpers", type: .list(.object(PotentialHelper.selections))),
        GraphQLField("helper", type: .object(Helper.selections)),
        GraphQLField("blockedUsers", type: .list(.object(BlockedUser.selections))),
        GraphQLField("owner", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, title: String, requester: Requester, description: String, duration: String, latitude: Double, longitude: Double, potentialHelpers: [PotentialHelper?]? = nil, helper: Helper? = nil, blockedUsers: [BlockedUser?]? = nil, owner: String? = nil) {
        self.init(snapshot: ["__typename": "Request", "id": id, "title": title, "requester": requester.snapshot, "description": description, "duration": duration, "latitude": latitude, "longitude": longitude, "potentialHelpers": potentialHelpers.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "helper": helper.flatMap { $0.snapshot }, "blockedUsers": blockedUsers.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "owner": owner])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var title: String {
        get {
          return snapshot["title"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "title")
        }
      }

      public var requester: Requester {
        get {
          return Requester(snapshot: snapshot["requester"]! as! Snapshot)
        }
        set {
          snapshot.updateValue(newValue.snapshot, forKey: "requester")
        }
      }

      public var description: String {
        get {
          return snapshot["description"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "description")
        }
      }

      public var duration: String {
        get {
          return snapshot["duration"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "duration")
        }
      }

      public var latitude: Double {
        get {
          return snapshot["latitude"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "latitude")
        }
      }

      public var longitude: Double {
        get {
          return snapshot["longitude"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "longitude")
        }
      }

      public var potentialHelpers: [PotentialHelper?]? {
        get {
          return (snapshot["potentialHelpers"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { PotentialHelper(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "potentialHelpers")
        }
      }

      public var helper: Helper? {
        get {
          return (snapshot["helper"] as? Snapshot).flatMap { Helper(snapshot: $0) }
        }
        set {
          snapshot.updateValue(newValue?.snapshot, forKey: "helper")
        }
      }

      public var blockedUsers: [BlockedUser?]? {
        get {
          return (snapshot["blockedUsers"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { BlockedUser(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "blockedUsers")
        }
      }

      public var owner: String? {
        get {
          return snapshot["owner"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "owner")
        }
      }

      public struct Requester: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct PotentialHelper: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct Helper: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct BlockedUser: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }
    }
  }
}

public final class DeleteRequestMutation: GraphQLMutation {
  public static let operationString =
    "mutation DeleteRequest($input: DeleteRequestInput!) {\n  deleteRequest(input: $input) {\n    __typename\n    id\n    title\n    requester {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    description\n    duration\n    latitude\n    longitude\n    potentialHelpers {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    helper {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    blockedUsers {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    owner\n  }\n}"

  public var input: DeleteRequestInput

  public init(input: DeleteRequestInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteRequest", arguments: ["input": GraphQLVariable("input")], type: .object(DeleteRequest.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(deleteRequest: DeleteRequest? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "deleteRequest": deleteRequest.flatMap { $0.snapshot }])
    }

    public var deleteRequest: DeleteRequest? {
      get {
        return (snapshot["deleteRequest"] as? Snapshot).flatMap { DeleteRequest(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "deleteRequest")
      }
    }

    public struct DeleteRequest: GraphQLSelectionSet {
      public static let possibleTypes = ["Request"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("title", type: .nonNull(.scalar(String.self))),
        GraphQLField("requester", type: .nonNull(.object(Requester.selections))),
        GraphQLField("description", type: .nonNull(.scalar(String.self))),
        GraphQLField("duration", type: .nonNull(.scalar(String.self))),
        GraphQLField("latitude", type: .nonNull(.scalar(Double.self))),
        GraphQLField("longitude", type: .nonNull(.scalar(Double.self))),
        GraphQLField("potentialHelpers", type: .list(.object(PotentialHelper.selections))),
        GraphQLField("helper", type: .object(Helper.selections)),
        GraphQLField("blockedUsers", type: .list(.object(BlockedUser.selections))),
        GraphQLField("owner", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, title: String, requester: Requester, description: String, duration: String, latitude: Double, longitude: Double, potentialHelpers: [PotentialHelper?]? = nil, helper: Helper? = nil, blockedUsers: [BlockedUser?]? = nil, owner: String? = nil) {
        self.init(snapshot: ["__typename": "Request", "id": id, "title": title, "requester": requester.snapshot, "description": description, "duration": duration, "latitude": latitude, "longitude": longitude, "potentialHelpers": potentialHelpers.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "helper": helper.flatMap { $0.snapshot }, "blockedUsers": blockedUsers.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "owner": owner])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var title: String {
        get {
          return snapshot["title"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "title")
        }
      }

      public var requester: Requester {
        get {
          return Requester(snapshot: snapshot["requester"]! as! Snapshot)
        }
        set {
          snapshot.updateValue(newValue.snapshot, forKey: "requester")
        }
      }

      public var description: String {
        get {
          return snapshot["description"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "description")
        }
      }

      public var duration: String {
        get {
          return snapshot["duration"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "duration")
        }
      }

      public var latitude: Double {
        get {
          return snapshot["latitude"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "latitude")
        }
      }

      public var longitude: Double {
        get {
          return snapshot["longitude"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "longitude")
        }
      }

      public var potentialHelpers: [PotentialHelper?]? {
        get {
          return (snapshot["potentialHelpers"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { PotentialHelper(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "potentialHelpers")
        }
      }

      public var helper: Helper? {
        get {
          return (snapshot["helper"] as? Snapshot).flatMap { Helper(snapshot: $0) }
        }
        set {
          snapshot.updateValue(newValue?.snapshot, forKey: "helper")
        }
      }

      public var blockedUsers: [BlockedUser?]? {
        get {
          return (snapshot["blockedUsers"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { BlockedUser(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "blockedUsers")
        }
      }

      public var owner: String? {
        get {
          return snapshot["owner"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "owner")
        }
      }

      public struct Requester: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct PotentialHelper: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct Helper: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct BlockedUser: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }
    }
  }
}

public final class GetUserQuery: GraphQLQuery {
  public static let operationString =
    "query GetUser($id: ID!) {\n  getUser(id: $id) {\n    __typename\n    id\n    email\n    phoneNumber\n    firstName\n    lastName\n    preferredContact\n    description\n    requests {\n      __typename\n      items {\n        __typename\n        id\n        title\n        description\n        duration\n        latitude\n        longitude\n        owner\n      }\n      nextToken\n    }\n    helperRatings\n    imageUrl\n    owner\n  }\n}"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getUser", arguments: ["id": GraphQLVariable("id")], type: .object(GetUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(getUser: GetUser? = nil) {
      self.init(snapshot: ["__typename": "Query", "getUser": getUser.flatMap { $0.snapshot }])
    }

    public var getUser: GetUser? {
      get {
        return (snapshot["getUser"] as? Snapshot).flatMap { GetUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "getUser")
      }
    }

    public struct GetUser: GraphQLSelectionSet {
      public static let possibleTypes = ["User"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("email", type: .nonNull(.scalar(String.self))),
        GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
        GraphQLField("firstName", type: .scalar(String.self)),
        GraphQLField("lastName", type: .scalar(String.self)),
        GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
        GraphQLField("description", type: .scalar(String.self)),
        GraphQLField("requests", type: .object(Request.selections)),
        GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
        GraphQLField("imageUrl", type: .scalar(String.self)),
        GraphQLField("owner", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
        self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var email: String {
        get {
          return snapshot["email"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "email")
        }
      }

      public var phoneNumber: String {
        get {
          return snapshot["phoneNumber"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "phoneNumber")
        }
      }

      public var firstName: String? {
        get {
          return snapshot["firstName"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "firstName")
        }
      }

      public var lastName: String? {
        get {
          return snapshot["lastName"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "lastName")
        }
      }

      public var preferredContact: [PreferredContact?]? {
        get {
          return snapshot["preferredContact"] as? [PreferredContact?]
        }
        set {
          snapshot.updateValue(newValue, forKey: "preferredContact")
        }
      }

      public var description: String? {
        get {
          return snapshot["description"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "description")
        }
      }

      public var requests: Request? {
        get {
          return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
        }
        set {
          snapshot.updateValue(newValue?.snapshot, forKey: "requests")
        }
      }

      public var helperRatings: [Double?]? {
        get {
          return snapshot["helperRatings"] as? [Double?]
        }
        set {
          snapshot.updateValue(newValue, forKey: "helperRatings")
        }
      }

      public var imageUrl: String? {
        get {
          return snapshot["imageUrl"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "imageUrl")
        }
      }

      public var owner: String? {
        get {
          return snapshot["owner"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "owner")
        }
      }

      public struct Request: GraphQLSelectionSet {
        public static let possibleTypes = ["ModelRequestConnection"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("items", type: .list(.object(Item.selections))),
          GraphQLField("nextToken", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(items: [Item?]? = nil, nextToken: String? = nil) {
          self.init(snapshot: ["__typename": "ModelRequestConnection", "items": items.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "nextToken": nextToken])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var items: [Item?]? {
          get {
            return (snapshot["items"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { Item(snapshot: $0) } } }
          }
          set {
            snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "items")
          }
        }

        public var nextToken: String? {
          get {
            return snapshot["nextToken"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "nextToken")
          }
        }

        public struct Item: GraphQLSelectionSet {
          public static let possibleTypes = ["Request"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
            GraphQLField("title", type: .nonNull(.scalar(String.self))),
            GraphQLField("description", type: .nonNull(.scalar(String.self))),
            GraphQLField("duration", type: .nonNull(.scalar(String.self))),
            GraphQLField("latitude", type: .nonNull(.scalar(Double.self))),
            GraphQLField("longitude", type: .nonNull(.scalar(Double.self))),
            GraphQLField("owner", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(id: GraphQLID, title: String, description: String, duration: String, latitude: Double, longitude: Double, owner: String? = nil) {
            self.init(snapshot: ["__typename": "Request", "id": id, "title": title, "description": description, "duration": duration, "latitude": latitude, "longitude": longitude, "owner": owner])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID {
            get {
              return snapshot["id"]! as! GraphQLID
            }
            set {
              snapshot.updateValue(newValue, forKey: "id")
            }
          }

          public var title: String {
            get {
              return snapshot["title"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "title")
            }
          }

          public var description: String {
            get {
              return snapshot["description"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "description")
            }
          }

          public var duration: String {
            get {
              return snapshot["duration"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "duration")
            }
          }

          public var latitude: Double {
            get {
              return snapshot["latitude"]! as! Double
            }
            set {
              snapshot.updateValue(newValue, forKey: "latitude")
            }
          }

          public var longitude: Double {
            get {
              return snapshot["longitude"]! as! Double
            }
            set {
              snapshot.updateValue(newValue, forKey: "longitude")
            }
          }

          public var owner: String? {
            get {
              return snapshot["owner"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "owner")
            }
          }
        }
      }
    }
  }
}

public final class ListUsersQuery: GraphQLQuery {
  public static let operationString =
    "query ListUsers($filter: ModelUserFilterInput, $limit: Int, $nextToken: String) {\n  listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {\n    __typename\n    items {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    nextToken\n  }\n}"

  public var filter: ModelUserFilterInput?
  public var limit: Int?
  public var nextToken: String?

  public init(filter: ModelUserFilterInput? = nil, limit: Int? = nil, nextToken: String? = nil) {
    self.filter = filter
    self.limit = limit
    self.nextToken = nextToken
  }

  public var variables: GraphQLMap? {
    return ["filter": filter, "limit": limit, "nextToken": nextToken]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listUsers", arguments: ["filter": GraphQLVariable("filter"), "limit": GraphQLVariable("limit"), "nextToken": GraphQLVariable("nextToken")], type: .object(ListUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(listUsers: ListUser? = nil) {
      self.init(snapshot: ["__typename": "Query", "listUsers": listUsers.flatMap { $0.snapshot }])
    }

    public var listUsers: ListUser? {
      get {
        return (snapshot["listUsers"] as? Snapshot).flatMap { ListUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "listUsers")
      }
    }

    public struct ListUser: GraphQLSelectionSet {
      public static let possibleTypes = ["ModelUserConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
        GraphQLField("nextToken", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(items: [Item?]? = nil, nextToken: String? = nil) {
        self.init(snapshot: ["__typename": "ModelUserConnection", "items": items.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "nextToken": nextToken])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (snapshot["items"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { Item(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "items")
        }
      }

      public var nextToken: String? {
        get {
          return snapshot["nextToken"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "nextToken")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }
    }
  }
}

public final class GetRequestQuery: GraphQLQuery {
  public static let operationString =
    "query GetRequest($id: ID!) {\n  getRequest(id: $id) {\n    __typename\n    id\n    title\n    requester {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    description\n    duration\n    latitude\n    longitude\n    potentialHelpers {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    helper {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    blockedUsers {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    owner\n  }\n}"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getRequest", arguments: ["id": GraphQLVariable("id")], type: .object(GetRequest.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(getRequest: GetRequest? = nil) {
      self.init(snapshot: ["__typename": "Query", "getRequest": getRequest.flatMap { $0.snapshot }])
    }

    public var getRequest: GetRequest? {
      get {
        return (snapshot["getRequest"] as? Snapshot).flatMap { GetRequest(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "getRequest")
      }
    }

    public struct GetRequest: GraphQLSelectionSet {
      public static let possibleTypes = ["Request"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("title", type: .nonNull(.scalar(String.self))),
        GraphQLField("requester", type: .nonNull(.object(Requester.selections))),
        GraphQLField("description", type: .nonNull(.scalar(String.self))),
        GraphQLField("duration", type: .nonNull(.scalar(String.self))),
        GraphQLField("latitude", type: .nonNull(.scalar(Double.self))),
        GraphQLField("longitude", type: .nonNull(.scalar(Double.self))),
        GraphQLField("potentialHelpers", type: .list(.object(PotentialHelper.selections))),
        GraphQLField("helper", type: .object(Helper.selections)),
        GraphQLField("blockedUsers", type: .list(.object(BlockedUser.selections))),
        GraphQLField("owner", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, title: String, requester: Requester, description: String, duration: String, latitude: Double, longitude: Double, potentialHelpers: [PotentialHelper?]? = nil, helper: Helper? = nil, blockedUsers: [BlockedUser?]? = nil, owner: String? = nil) {
        self.init(snapshot: ["__typename": "Request", "id": id, "title": title, "requester": requester.snapshot, "description": description, "duration": duration, "latitude": latitude, "longitude": longitude, "potentialHelpers": potentialHelpers.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "helper": helper.flatMap { $0.snapshot }, "blockedUsers": blockedUsers.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "owner": owner])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var title: String {
        get {
          return snapshot["title"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "title")
        }
      }

      public var requester: Requester {
        get {
          return Requester(snapshot: snapshot["requester"]! as! Snapshot)
        }
        set {
          snapshot.updateValue(newValue.snapshot, forKey: "requester")
        }
      }

      public var description: String {
        get {
          return snapshot["description"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "description")
        }
      }

      public var duration: String {
        get {
          return snapshot["duration"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "duration")
        }
      }

      public var latitude: Double {
        get {
          return snapshot["latitude"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "latitude")
        }
      }

      public var longitude: Double {
        get {
          return snapshot["longitude"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "longitude")
        }
      }

      public var potentialHelpers: [PotentialHelper?]? {
        get {
          return (snapshot["potentialHelpers"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { PotentialHelper(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "potentialHelpers")
        }
      }

      public var helper: Helper? {
        get {
          return (snapshot["helper"] as? Snapshot).flatMap { Helper(snapshot: $0) }
        }
        set {
          snapshot.updateValue(newValue?.snapshot, forKey: "helper")
        }
      }

      public var blockedUsers: [BlockedUser?]? {
        get {
          return (snapshot["blockedUsers"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { BlockedUser(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "blockedUsers")
        }
      }

      public var owner: String? {
        get {
          return snapshot["owner"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "owner")
        }
      }

      public struct Requester: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct PotentialHelper: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct Helper: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct BlockedUser: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }
    }
  }
}

public final class ListRequestsQuery: GraphQLQuery {
  public static let operationString =
    "query ListRequests($filter: ModelRequestFilterInput, $limit: Int, $nextToken: String) {\n  listRequests(filter: $filter, limit: $limit, nextToken: $nextToken) {\n    __typename\n    items {\n      __typename\n      id\n      title\n      requester {\n        __typename\n        id\n        email\n        phoneNumber\n        firstName\n        lastName\n        preferredContact\n        description\n        helperRatings\n        imageUrl\n        owner\n      }\n      description\n      duration\n      latitude\n      longitude\n      potentialHelpers {\n        __typename\n        id\n        email\n        phoneNumber\n        firstName\n        lastName\n        preferredContact\n        description\n        helperRatings\n        imageUrl\n        owner\n      }\n      helper {\n        __typename\n        id\n        email\n        phoneNumber\n        firstName\n        lastName\n        preferredContact\n        description\n        helperRatings\n        imageUrl\n        owner\n      }\n      blockedUsers {\n        __typename\n        id\n        email\n        phoneNumber\n        firstName\n        lastName\n        preferredContact\n        description\n        helperRatings\n        imageUrl\n        owner\n      }\n      owner\n    }\n    nextToken\n  }\n}"

  public var filter: ModelRequestFilterInput?
  public var limit: Int?
  public var nextToken: String?

  public init(filter: ModelRequestFilterInput? = nil, limit: Int? = nil, nextToken: String? = nil) {
    self.filter = filter
    self.limit = limit
    self.nextToken = nextToken
  }

  public var variables: GraphQLMap? {
    return ["filter": filter, "limit": limit, "nextToken": nextToken]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listRequests", arguments: ["filter": GraphQLVariable("filter"), "limit": GraphQLVariable("limit"), "nextToken": GraphQLVariable("nextToken")], type: .object(ListRequest.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(listRequests: ListRequest? = nil) {
      self.init(snapshot: ["__typename": "Query", "listRequests": listRequests.flatMap { $0.snapshot }])
    }

    public var listRequests: ListRequest? {
      get {
        return (snapshot["listRequests"] as? Snapshot).flatMap { ListRequest(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "listRequests")
      }
    }

    public struct ListRequest: GraphQLSelectionSet {
      public static let possibleTypes = ["ModelRequestConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
        GraphQLField("nextToken", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(items: [Item?]? = nil, nextToken: String? = nil) {
        self.init(snapshot: ["__typename": "ModelRequestConnection", "items": items.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "nextToken": nextToken])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (snapshot["items"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { Item(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "items")
        }
      }

      public var nextToken: String? {
        get {
          return snapshot["nextToken"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "nextToken")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes = ["Request"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("title", type: .nonNull(.scalar(String.self))),
          GraphQLField("requester", type: .nonNull(.object(Requester.selections))),
          GraphQLField("description", type: .nonNull(.scalar(String.self))),
          GraphQLField("duration", type: .nonNull(.scalar(String.self))),
          GraphQLField("latitude", type: .nonNull(.scalar(Double.self))),
          GraphQLField("longitude", type: .nonNull(.scalar(Double.self))),
          GraphQLField("potentialHelpers", type: .list(.object(PotentialHelper.selections))),
          GraphQLField("helper", type: .object(Helper.selections)),
          GraphQLField("blockedUsers", type: .list(.object(BlockedUser.selections))),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, title: String, requester: Requester, description: String, duration: String, latitude: Double, longitude: Double, potentialHelpers: [PotentialHelper?]? = nil, helper: Helper? = nil, blockedUsers: [BlockedUser?]? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "Request", "id": id, "title": title, "requester": requester.snapshot, "description": description, "duration": duration, "latitude": latitude, "longitude": longitude, "potentialHelpers": potentialHelpers.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "helper": helper.flatMap { $0.snapshot }, "blockedUsers": blockedUsers.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var title: String {
          get {
            return snapshot["title"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "title")
          }
        }

        public var requester: Requester {
          get {
            return Requester(snapshot: snapshot["requester"]! as! Snapshot)
          }
          set {
            snapshot.updateValue(newValue.snapshot, forKey: "requester")
          }
        }

        public var description: String {
          get {
            return snapshot["description"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var duration: String {
          get {
            return snapshot["duration"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "duration")
          }
        }

        public var latitude: Double {
          get {
            return snapshot["latitude"]! as! Double
          }
          set {
            snapshot.updateValue(newValue, forKey: "latitude")
          }
        }

        public var longitude: Double {
          get {
            return snapshot["longitude"]! as! Double
          }
          set {
            snapshot.updateValue(newValue, forKey: "longitude")
          }
        }

        public var potentialHelpers: [PotentialHelper?]? {
          get {
            return (snapshot["potentialHelpers"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { PotentialHelper(snapshot: $0) } } }
          }
          set {
            snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "potentialHelpers")
          }
        }

        public var helper: Helper? {
          get {
            return (snapshot["helper"] as? Snapshot).flatMap { Helper(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "helper")
          }
        }

        public var blockedUsers: [BlockedUser?]? {
          get {
            return (snapshot["blockedUsers"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { BlockedUser(snapshot: $0) } } }
          }
          set {
            snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "blockedUsers")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Requester: GraphQLSelectionSet {
          public static let possibleTypes = ["User"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
            GraphQLField("email", type: .nonNull(.scalar(String.self))),
            GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
            GraphQLField("firstName", type: .scalar(String.self)),
            GraphQLField("lastName", type: .scalar(String.self)),
            GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
            GraphQLField("description", type: .scalar(String.self)),
            GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
            GraphQLField("imageUrl", type: .scalar(String.self)),
            GraphQLField("owner", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
            self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID {
            get {
              return snapshot["id"]! as! GraphQLID
            }
            set {
              snapshot.updateValue(newValue, forKey: "id")
            }
          }

          public var email: String {
            get {
              return snapshot["email"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "email")
            }
          }

          public var phoneNumber: String {
            get {
              return snapshot["phoneNumber"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "phoneNumber")
            }
          }

          public var firstName: String? {
            get {
              return snapshot["firstName"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "firstName")
            }
          }

          public var lastName: String? {
            get {
              return snapshot["lastName"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "lastName")
            }
          }

          public var preferredContact: [PreferredContact?]? {
            get {
              return snapshot["preferredContact"] as? [PreferredContact?]
            }
            set {
              snapshot.updateValue(newValue, forKey: "preferredContact")
            }
          }

          public var description: String? {
            get {
              return snapshot["description"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "description")
            }
          }

          public var helperRatings: [Double?]? {
            get {
              return snapshot["helperRatings"] as? [Double?]
            }
            set {
              snapshot.updateValue(newValue, forKey: "helperRatings")
            }
          }

          public var imageUrl: String? {
            get {
              return snapshot["imageUrl"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "imageUrl")
            }
          }

          public var owner: String? {
            get {
              return snapshot["owner"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "owner")
            }
          }
        }

        public struct PotentialHelper: GraphQLSelectionSet {
          public static let possibleTypes = ["User"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
            GraphQLField("email", type: .nonNull(.scalar(String.self))),
            GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
            GraphQLField("firstName", type: .scalar(String.self)),
            GraphQLField("lastName", type: .scalar(String.self)),
            GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
            GraphQLField("description", type: .scalar(String.self)),
            GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
            GraphQLField("imageUrl", type: .scalar(String.self)),
            GraphQLField("owner", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
            self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID {
            get {
              return snapshot["id"]! as! GraphQLID
            }
            set {
              snapshot.updateValue(newValue, forKey: "id")
            }
          }

          public var email: String {
            get {
              return snapshot["email"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "email")
            }
          }

          public var phoneNumber: String {
            get {
              return snapshot["phoneNumber"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "phoneNumber")
            }
          }

          public var firstName: String? {
            get {
              return snapshot["firstName"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "firstName")
            }
          }

          public var lastName: String? {
            get {
              return snapshot["lastName"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "lastName")
            }
          }

          public var preferredContact: [PreferredContact?]? {
            get {
              return snapshot["preferredContact"] as? [PreferredContact?]
            }
            set {
              snapshot.updateValue(newValue, forKey: "preferredContact")
            }
          }

          public var description: String? {
            get {
              return snapshot["description"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "description")
            }
          }

          public var helperRatings: [Double?]? {
            get {
              return snapshot["helperRatings"] as? [Double?]
            }
            set {
              snapshot.updateValue(newValue, forKey: "helperRatings")
            }
          }

          public var imageUrl: String? {
            get {
              return snapshot["imageUrl"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "imageUrl")
            }
          }

          public var owner: String? {
            get {
              return snapshot["owner"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "owner")
            }
          }
        }

        public struct Helper: GraphQLSelectionSet {
          public static let possibleTypes = ["User"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
            GraphQLField("email", type: .nonNull(.scalar(String.self))),
            GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
            GraphQLField("firstName", type: .scalar(String.self)),
            GraphQLField("lastName", type: .scalar(String.self)),
            GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
            GraphQLField("description", type: .scalar(String.self)),
            GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
            GraphQLField("imageUrl", type: .scalar(String.self)),
            GraphQLField("owner", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
            self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID {
            get {
              return snapshot["id"]! as! GraphQLID
            }
            set {
              snapshot.updateValue(newValue, forKey: "id")
            }
          }

          public var email: String {
            get {
              return snapshot["email"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "email")
            }
          }

          public var phoneNumber: String {
            get {
              return snapshot["phoneNumber"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "phoneNumber")
            }
          }

          public var firstName: String? {
            get {
              return snapshot["firstName"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "firstName")
            }
          }

          public var lastName: String? {
            get {
              return snapshot["lastName"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "lastName")
            }
          }

          public var preferredContact: [PreferredContact?]? {
            get {
              return snapshot["preferredContact"] as? [PreferredContact?]
            }
            set {
              snapshot.updateValue(newValue, forKey: "preferredContact")
            }
          }

          public var description: String? {
            get {
              return snapshot["description"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "description")
            }
          }

          public var helperRatings: [Double?]? {
            get {
              return snapshot["helperRatings"] as? [Double?]
            }
            set {
              snapshot.updateValue(newValue, forKey: "helperRatings")
            }
          }

          public var imageUrl: String? {
            get {
              return snapshot["imageUrl"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "imageUrl")
            }
          }

          public var owner: String? {
            get {
              return snapshot["owner"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "owner")
            }
          }
        }

        public struct BlockedUser: GraphQLSelectionSet {
          public static let possibleTypes = ["User"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
            GraphQLField("email", type: .nonNull(.scalar(String.self))),
            GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
            GraphQLField("firstName", type: .scalar(String.self)),
            GraphQLField("lastName", type: .scalar(String.self)),
            GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
            GraphQLField("description", type: .scalar(String.self)),
            GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
            GraphQLField("imageUrl", type: .scalar(String.self)),
            GraphQLField("owner", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
            self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID {
            get {
              return snapshot["id"]! as! GraphQLID
            }
            set {
              snapshot.updateValue(newValue, forKey: "id")
            }
          }

          public var email: String {
            get {
              return snapshot["email"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "email")
            }
          }

          public var phoneNumber: String {
            get {
              return snapshot["phoneNumber"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "phoneNumber")
            }
          }

          public var firstName: String? {
            get {
              return snapshot["firstName"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "firstName")
            }
          }

          public var lastName: String? {
            get {
              return snapshot["lastName"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "lastName")
            }
          }

          public var preferredContact: [PreferredContact?]? {
            get {
              return snapshot["preferredContact"] as? [PreferredContact?]
            }
            set {
              snapshot.updateValue(newValue, forKey: "preferredContact")
            }
          }

          public var description: String? {
            get {
              return snapshot["description"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "description")
            }
          }

          public var helperRatings: [Double?]? {
            get {
              return snapshot["helperRatings"] as? [Double?]
            }
            set {
              snapshot.updateValue(newValue, forKey: "helperRatings")
            }
          }

          public var imageUrl: String? {
            get {
              return snapshot["imageUrl"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "imageUrl")
            }
          }

          public var owner: String? {
            get {
              return snapshot["owner"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "owner")
            }
          }
        }
      }
    }
  }
}

public final class OnCreateUserSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnCreateUser($owner: String!) {\n  onCreateUser(owner: $owner) {\n    __typename\n    id\n    email\n    phoneNumber\n    firstName\n    lastName\n    preferredContact\n    description\n    requests {\n      __typename\n      items {\n        __typename\n        id\n        title\n        description\n        duration\n        latitude\n        longitude\n        owner\n      }\n      nextToken\n    }\n    helperRatings\n    imageUrl\n    owner\n  }\n}"

  public var owner: String

  public init(owner: String) {
    self.owner = owner
  }

  public var variables: GraphQLMap? {
    return ["owner": owner]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onCreateUser", arguments: ["owner": GraphQLVariable("owner")], type: .object(OnCreateUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onCreateUser: OnCreateUser? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onCreateUser": onCreateUser.flatMap { $0.snapshot }])
    }

    public var onCreateUser: OnCreateUser? {
      get {
        return (snapshot["onCreateUser"] as? Snapshot).flatMap { OnCreateUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onCreateUser")
      }
    }

    public struct OnCreateUser: GraphQLSelectionSet {
      public static let possibleTypes = ["User"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("email", type: .nonNull(.scalar(String.self))),
        GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
        GraphQLField("firstName", type: .scalar(String.self)),
        GraphQLField("lastName", type: .scalar(String.self)),
        GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
        GraphQLField("description", type: .scalar(String.self)),
        GraphQLField("requests", type: .object(Request.selections)),
        GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
        GraphQLField("imageUrl", type: .scalar(String.self)),
        GraphQLField("owner", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
        self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var email: String {
        get {
          return snapshot["email"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "email")
        }
      }

      public var phoneNumber: String {
        get {
          return snapshot["phoneNumber"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "phoneNumber")
        }
      }

      public var firstName: String? {
        get {
          return snapshot["firstName"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "firstName")
        }
      }

      public var lastName: String? {
        get {
          return snapshot["lastName"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "lastName")
        }
      }

      public var preferredContact: [PreferredContact?]? {
        get {
          return snapshot["preferredContact"] as? [PreferredContact?]
        }
        set {
          snapshot.updateValue(newValue, forKey: "preferredContact")
        }
      }

      public var description: String? {
        get {
          return snapshot["description"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "description")
        }
      }

      public var requests: Request? {
        get {
          return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
        }
        set {
          snapshot.updateValue(newValue?.snapshot, forKey: "requests")
        }
      }

      public var helperRatings: [Double?]? {
        get {
          return snapshot["helperRatings"] as? [Double?]
        }
        set {
          snapshot.updateValue(newValue, forKey: "helperRatings")
        }
      }

      public var imageUrl: String? {
        get {
          return snapshot["imageUrl"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "imageUrl")
        }
      }

      public var owner: String? {
        get {
          return snapshot["owner"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "owner")
        }
      }

      public struct Request: GraphQLSelectionSet {
        public static let possibleTypes = ["ModelRequestConnection"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("items", type: .list(.object(Item.selections))),
          GraphQLField("nextToken", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(items: [Item?]? = nil, nextToken: String? = nil) {
          self.init(snapshot: ["__typename": "ModelRequestConnection", "items": items.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "nextToken": nextToken])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var items: [Item?]? {
          get {
            return (snapshot["items"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { Item(snapshot: $0) } } }
          }
          set {
            snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "items")
          }
        }

        public var nextToken: String? {
          get {
            return snapshot["nextToken"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "nextToken")
          }
        }

        public struct Item: GraphQLSelectionSet {
          public static let possibleTypes = ["Request"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
            GraphQLField("title", type: .nonNull(.scalar(String.self))),
            GraphQLField("description", type: .nonNull(.scalar(String.self))),
            GraphQLField("duration", type: .nonNull(.scalar(String.self))),
            GraphQLField("latitude", type: .nonNull(.scalar(Double.self))),
            GraphQLField("longitude", type: .nonNull(.scalar(Double.self))),
            GraphQLField("owner", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(id: GraphQLID, title: String, description: String, duration: String, latitude: Double, longitude: Double, owner: String? = nil) {
            self.init(snapshot: ["__typename": "Request", "id": id, "title": title, "description": description, "duration": duration, "latitude": latitude, "longitude": longitude, "owner": owner])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID {
            get {
              return snapshot["id"]! as! GraphQLID
            }
            set {
              snapshot.updateValue(newValue, forKey: "id")
            }
          }

          public var title: String {
            get {
              return snapshot["title"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "title")
            }
          }

          public var description: String {
            get {
              return snapshot["description"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "description")
            }
          }

          public var duration: String {
            get {
              return snapshot["duration"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "duration")
            }
          }

          public var latitude: Double {
            get {
              return snapshot["latitude"]! as! Double
            }
            set {
              snapshot.updateValue(newValue, forKey: "latitude")
            }
          }

          public var longitude: Double {
            get {
              return snapshot["longitude"]! as! Double
            }
            set {
              snapshot.updateValue(newValue, forKey: "longitude")
            }
          }

          public var owner: String? {
            get {
              return snapshot["owner"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "owner")
            }
          }
        }
      }
    }
  }
}

public final class OnUpdateUserSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnUpdateUser($owner: String!) {\n  onUpdateUser(owner: $owner) {\n    __typename\n    id\n    email\n    phoneNumber\n    firstName\n    lastName\n    preferredContact\n    description\n    requests {\n      __typename\n      items {\n        __typename\n        id\n        title\n        description\n        duration\n        latitude\n        longitude\n        owner\n      }\n      nextToken\n    }\n    helperRatings\n    imageUrl\n    owner\n  }\n}"

  public var owner: String

  public init(owner: String) {
    self.owner = owner
  }

  public var variables: GraphQLMap? {
    return ["owner": owner]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onUpdateUser", arguments: ["owner": GraphQLVariable("owner")], type: .object(OnUpdateUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onUpdateUser: OnUpdateUser? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onUpdateUser": onUpdateUser.flatMap { $0.snapshot }])
    }

    public var onUpdateUser: OnUpdateUser? {
      get {
        return (snapshot["onUpdateUser"] as? Snapshot).flatMap { OnUpdateUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onUpdateUser")
      }
    }

    public struct OnUpdateUser: GraphQLSelectionSet {
      public static let possibleTypes = ["User"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("email", type: .nonNull(.scalar(String.self))),
        GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
        GraphQLField("firstName", type: .scalar(String.self)),
        GraphQLField("lastName", type: .scalar(String.self)),
        GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
        GraphQLField("description", type: .scalar(String.self)),
        GraphQLField("requests", type: .object(Request.selections)),
        GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
        GraphQLField("imageUrl", type: .scalar(String.self)),
        GraphQLField("owner", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
        self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var email: String {
        get {
          return snapshot["email"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "email")
        }
      }

      public var phoneNumber: String {
        get {
          return snapshot["phoneNumber"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "phoneNumber")
        }
      }

      public var firstName: String? {
        get {
          return snapshot["firstName"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "firstName")
        }
      }

      public var lastName: String? {
        get {
          return snapshot["lastName"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "lastName")
        }
      }

      public var preferredContact: [PreferredContact?]? {
        get {
          return snapshot["preferredContact"] as? [PreferredContact?]
        }
        set {
          snapshot.updateValue(newValue, forKey: "preferredContact")
        }
      }

      public var description: String? {
        get {
          return snapshot["description"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "description")
        }
      }

      public var requests: Request? {
        get {
          return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
        }
        set {
          snapshot.updateValue(newValue?.snapshot, forKey: "requests")
        }
      }

      public var helperRatings: [Double?]? {
        get {
          return snapshot["helperRatings"] as? [Double?]
        }
        set {
          snapshot.updateValue(newValue, forKey: "helperRatings")
        }
      }

      public var imageUrl: String? {
        get {
          return snapshot["imageUrl"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "imageUrl")
        }
      }

      public var owner: String? {
        get {
          return snapshot["owner"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "owner")
        }
      }

      public struct Request: GraphQLSelectionSet {
        public static let possibleTypes = ["ModelRequestConnection"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("items", type: .list(.object(Item.selections))),
          GraphQLField("nextToken", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(items: [Item?]? = nil, nextToken: String? = nil) {
          self.init(snapshot: ["__typename": "ModelRequestConnection", "items": items.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "nextToken": nextToken])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var items: [Item?]? {
          get {
            return (snapshot["items"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { Item(snapshot: $0) } } }
          }
          set {
            snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "items")
          }
        }

        public var nextToken: String? {
          get {
            return snapshot["nextToken"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "nextToken")
          }
        }

        public struct Item: GraphQLSelectionSet {
          public static let possibleTypes = ["Request"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
            GraphQLField("title", type: .nonNull(.scalar(String.self))),
            GraphQLField("description", type: .nonNull(.scalar(String.self))),
            GraphQLField("duration", type: .nonNull(.scalar(String.self))),
            GraphQLField("latitude", type: .nonNull(.scalar(Double.self))),
            GraphQLField("longitude", type: .nonNull(.scalar(Double.self))),
            GraphQLField("owner", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(id: GraphQLID, title: String, description: String, duration: String, latitude: Double, longitude: Double, owner: String? = nil) {
            self.init(snapshot: ["__typename": "Request", "id": id, "title": title, "description": description, "duration": duration, "latitude": latitude, "longitude": longitude, "owner": owner])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID {
            get {
              return snapshot["id"]! as! GraphQLID
            }
            set {
              snapshot.updateValue(newValue, forKey: "id")
            }
          }

          public var title: String {
            get {
              return snapshot["title"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "title")
            }
          }

          public var description: String {
            get {
              return snapshot["description"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "description")
            }
          }

          public var duration: String {
            get {
              return snapshot["duration"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "duration")
            }
          }

          public var latitude: Double {
            get {
              return snapshot["latitude"]! as! Double
            }
            set {
              snapshot.updateValue(newValue, forKey: "latitude")
            }
          }

          public var longitude: Double {
            get {
              return snapshot["longitude"]! as! Double
            }
            set {
              snapshot.updateValue(newValue, forKey: "longitude")
            }
          }

          public var owner: String? {
            get {
              return snapshot["owner"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "owner")
            }
          }
        }
      }
    }
  }
}

public final class OnDeleteUserSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnDeleteUser($owner: String!) {\n  onDeleteUser(owner: $owner) {\n    __typename\n    id\n    email\n    phoneNumber\n    firstName\n    lastName\n    preferredContact\n    description\n    requests {\n      __typename\n      items {\n        __typename\n        id\n        title\n        description\n        duration\n        latitude\n        longitude\n        owner\n      }\n      nextToken\n    }\n    helperRatings\n    imageUrl\n    owner\n  }\n}"

  public var owner: String

  public init(owner: String) {
    self.owner = owner
  }

  public var variables: GraphQLMap? {
    return ["owner": owner]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onDeleteUser", arguments: ["owner": GraphQLVariable("owner")], type: .object(OnDeleteUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onDeleteUser: OnDeleteUser? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onDeleteUser": onDeleteUser.flatMap { $0.snapshot }])
    }

    public var onDeleteUser: OnDeleteUser? {
      get {
        return (snapshot["onDeleteUser"] as? Snapshot).flatMap { OnDeleteUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onDeleteUser")
      }
    }

    public struct OnDeleteUser: GraphQLSelectionSet {
      public static let possibleTypes = ["User"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("email", type: .nonNull(.scalar(String.self))),
        GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
        GraphQLField("firstName", type: .scalar(String.self)),
        GraphQLField("lastName", type: .scalar(String.self)),
        GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
        GraphQLField("description", type: .scalar(String.self)),
        GraphQLField("requests", type: .object(Request.selections)),
        GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
        GraphQLField("imageUrl", type: .scalar(String.self)),
        GraphQLField("owner", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
        self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var email: String {
        get {
          return snapshot["email"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "email")
        }
      }

      public var phoneNumber: String {
        get {
          return snapshot["phoneNumber"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "phoneNumber")
        }
      }

      public var firstName: String? {
        get {
          return snapshot["firstName"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "firstName")
        }
      }

      public var lastName: String? {
        get {
          return snapshot["lastName"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "lastName")
        }
      }

      public var preferredContact: [PreferredContact?]? {
        get {
          return snapshot["preferredContact"] as? [PreferredContact?]
        }
        set {
          snapshot.updateValue(newValue, forKey: "preferredContact")
        }
      }

      public var description: String? {
        get {
          return snapshot["description"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "description")
        }
      }

      public var requests: Request? {
        get {
          return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
        }
        set {
          snapshot.updateValue(newValue?.snapshot, forKey: "requests")
        }
      }

      public var helperRatings: [Double?]? {
        get {
          return snapshot["helperRatings"] as? [Double?]
        }
        set {
          snapshot.updateValue(newValue, forKey: "helperRatings")
        }
      }

      public var imageUrl: String? {
        get {
          return snapshot["imageUrl"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "imageUrl")
        }
      }

      public var owner: String? {
        get {
          return snapshot["owner"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "owner")
        }
      }

      public struct Request: GraphQLSelectionSet {
        public static let possibleTypes = ["ModelRequestConnection"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("items", type: .list(.object(Item.selections))),
          GraphQLField("nextToken", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(items: [Item?]? = nil, nextToken: String? = nil) {
          self.init(snapshot: ["__typename": "ModelRequestConnection", "items": items.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "nextToken": nextToken])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var items: [Item?]? {
          get {
            return (snapshot["items"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { Item(snapshot: $0) } } }
          }
          set {
            snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "items")
          }
        }

        public var nextToken: String? {
          get {
            return snapshot["nextToken"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "nextToken")
          }
        }

        public struct Item: GraphQLSelectionSet {
          public static let possibleTypes = ["Request"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
            GraphQLField("title", type: .nonNull(.scalar(String.self))),
            GraphQLField("description", type: .nonNull(.scalar(String.self))),
            GraphQLField("duration", type: .nonNull(.scalar(String.self))),
            GraphQLField("latitude", type: .nonNull(.scalar(Double.self))),
            GraphQLField("longitude", type: .nonNull(.scalar(Double.self))),
            GraphQLField("owner", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(id: GraphQLID, title: String, description: String, duration: String, latitude: Double, longitude: Double, owner: String? = nil) {
            self.init(snapshot: ["__typename": "Request", "id": id, "title": title, "description": description, "duration": duration, "latitude": latitude, "longitude": longitude, "owner": owner])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID {
            get {
              return snapshot["id"]! as! GraphQLID
            }
            set {
              snapshot.updateValue(newValue, forKey: "id")
            }
          }

          public var title: String {
            get {
              return snapshot["title"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "title")
            }
          }

          public var description: String {
            get {
              return snapshot["description"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "description")
            }
          }

          public var duration: String {
            get {
              return snapshot["duration"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "duration")
            }
          }

          public var latitude: Double {
            get {
              return snapshot["latitude"]! as! Double
            }
            set {
              snapshot.updateValue(newValue, forKey: "latitude")
            }
          }

          public var longitude: Double {
            get {
              return snapshot["longitude"]! as! Double
            }
            set {
              snapshot.updateValue(newValue, forKey: "longitude")
            }
          }

          public var owner: String? {
            get {
              return snapshot["owner"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "owner")
            }
          }
        }
      }
    }
  }
}

public final class OnCreateRequestSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnCreateRequest($owner: String!) {\n  onCreateRequest(owner: $owner) {\n    __typename\n    id\n    title\n    requester {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    description\n    duration\n    latitude\n    longitude\n    potentialHelpers {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    helper {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    blockedUsers {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    owner\n  }\n}"

  public var owner: String

  public init(owner: String) {
    self.owner = owner
  }

  public var variables: GraphQLMap? {
    return ["owner": owner]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onCreateRequest", arguments: ["owner": GraphQLVariable("owner")], type: .object(OnCreateRequest.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onCreateRequest: OnCreateRequest? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onCreateRequest": onCreateRequest.flatMap { $0.snapshot }])
    }

    public var onCreateRequest: OnCreateRequest? {
      get {
        return (snapshot["onCreateRequest"] as? Snapshot).flatMap { OnCreateRequest(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onCreateRequest")
      }
    }

    public struct OnCreateRequest: GraphQLSelectionSet {
      public static let possibleTypes = ["Request"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("title", type: .nonNull(.scalar(String.self))),
        GraphQLField("requester", type: .nonNull(.object(Requester.selections))),
        GraphQLField("description", type: .nonNull(.scalar(String.self))),
        GraphQLField("duration", type: .nonNull(.scalar(String.self))),
        GraphQLField("latitude", type: .nonNull(.scalar(Double.self))),
        GraphQLField("longitude", type: .nonNull(.scalar(Double.self))),
        GraphQLField("potentialHelpers", type: .list(.object(PotentialHelper.selections))),
        GraphQLField("helper", type: .object(Helper.selections)),
        GraphQLField("blockedUsers", type: .list(.object(BlockedUser.selections))),
        GraphQLField("owner", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, title: String, requester: Requester, description: String, duration: String, latitude: Double, longitude: Double, potentialHelpers: [PotentialHelper?]? = nil, helper: Helper? = nil, blockedUsers: [BlockedUser?]? = nil, owner: String? = nil) {
        self.init(snapshot: ["__typename": "Request", "id": id, "title": title, "requester": requester.snapshot, "description": description, "duration": duration, "latitude": latitude, "longitude": longitude, "potentialHelpers": potentialHelpers.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "helper": helper.flatMap { $0.snapshot }, "blockedUsers": blockedUsers.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "owner": owner])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var title: String {
        get {
          return snapshot["title"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "title")
        }
      }

      public var requester: Requester {
        get {
          return Requester(snapshot: snapshot["requester"]! as! Snapshot)
        }
        set {
          snapshot.updateValue(newValue.snapshot, forKey: "requester")
        }
      }

      public var description: String {
        get {
          return snapshot["description"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "description")
        }
      }

      public var duration: String {
        get {
          return snapshot["duration"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "duration")
        }
      }

      public var latitude: Double {
        get {
          return snapshot["latitude"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "latitude")
        }
      }

      public var longitude: Double {
        get {
          return snapshot["longitude"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "longitude")
        }
      }

      public var potentialHelpers: [PotentialHelper?]? {
        get {
          return (snapshot["potentialHelpers"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { PotentialHelper(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "potentialHelpers")
        }
      }

      public var helper: Helper? {
        get {
          return (snapshot["helper"] as? Snapshot).flatMap { Helper(snapshot: $0) }
        }
        set {
          snapshot.updateValue(newValue?.snapshot, forKey: "helper")
        }
      }

      public var blockedUsers: [BlockedUser?]? {
        get {
          return (snapshot["blockedUsers"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { BlockedUser(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "blockedUsers")
        }
      }

      public var owner: String? {
        get {
          return snapshot["owner"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "owner")
        }
      }

      public struct Requester: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct PotentialHelper: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct Helper: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct BlockedUser: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }
    }
  }
}

public final class OnUpdateRequestSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnUpdateRequest($owner: String!) {\n  onUpdateRequest(owner: $owner) {\n    __typename\n    id\n    title\n    requester {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    description\n    duration\n    latitude\n    longitude\n    potentialHelpers {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    helper {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    blockedUsers {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    owner\n  }\n}"

  public var owner: String

  public init(owner: String) {
    self.owner = owner
  }

  public var variables: GraphQLMap? {
    return ["owner": owner]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onUpdateRequest", arguments: ["owner": GraphQLVariable("owner")], type: .object(OnUpdateRequest.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onUpdateRequest: OnUpdateRequest? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onUpdateRequest": onUpdateRequest.flatMap { $0.snapshot }])
    }

    public var onUpdateRequest: OnUpdateRequest? {
      get {
        return (snapshot["onUpdateRequest"] as? Snapshot).flatMap { OnUpdateRequest(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onUpdateRequest")
      }
    }

    public struct OnUpdateRequest: GraphQLSelectionSet {
      public static let possibleTypes = ["Request"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("title", type: .nonNull(.scalar(String.self))),
        GraphQLField("requester", type: .nonNull(.object(Requester.selections))),
        GraphQLField("description", type: .nonNull(.scalar(String.self))),
        GraphQLField("duration", type: .nonNull(.scalar(String.self))),
        GraphQLField("latitude", type: .nonNull(.scalar(Double.self))),
        GraphQLField("longitude", type: .nonNull(.scalar(Double.self))),
        GraphQLField("potentialHelpers", type: .list(.object(PotentialHelper.selections))),
        GraphQLField("helper", type: .object(Helper.selections)),
        GraphQLField("blockedUsers", type: .list(.object(BlockedUser.selections))),
        GraphQLField("owner", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, title: String, requester: Requester, description: String, duration: String, latitude: Double, longitude: Double, potentialHelpers: [PotentialHelper?]? = nil, helper: Helper? = nil, blockedUsers: [BlockedUser?]? = nil, owner: String? = nil) {
        self.init(snapshot: ["__typename": "Request", "id": id, "title": title, "requester": requester.snapshot, "description": description, "duration": duration, "latitude": latitude, "longitude": longitude, "potentialHelpers": potentialHelpers.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "helper": helper.flatMap { $0.snapshot }, "blockedUsers": blockedUsers.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "owner": owner])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var title: String {
        get {
          return snapshot["title"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "title")
        }
      }

      public var requester: Requester {
        get {
          return Requester(snapshot: snapshot["requester"]! as! Snapshot)
        }
        set {
          snapshot.updateValue(newValue.snapshot, forKey: "requester")
        }
      }

      public var description: String {
        get {
          return snapshot["description"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "description")
        }
      }

      public var duration: String {
        get {
          return snapshot["duration"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "duration")
        }
      }

      public var latitude: Double {
        get {
          return snapshot["latitude"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "latitude")
        }
      }

      public var longitude: Double {
        get {
          return snapshot["longitude"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "longitude")
        }
      }

      public var potentialHelpers: [PotentialHelper?]? {
        get {
          return (snapshot["potentialHelpers"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { PotentialHelper(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "potentialHelpers")
        }
      }

      public var helper: Helper? {
        get {
          return (snapshot["helper"] as? Snapshot).flatMap { Helper(snapshot: $0) }
        }
        set {
          snapshot.updateValue(newValue?.snapshot, forKey: "helper")
        }
      }

      public var blockedUsers: [BlockedUser?]? {
        get {
          return (snapshot["blockedUsers"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { BlockedUser(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "blockedUsers")
        }
      }

      public var owner: String? {
        get {
          return snapshot["owner"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "owner")
        }
      }

      public struct Requester: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct PotentialHelper: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct Helper: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct BlockedUser: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }
    }
  }
}

public final class OnDeleteRequestSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnDeleteRequest($owner: String!) {\n  onDeleteRequest(owner: $owner) {\n    __typename\n    id\n    title\n    requester {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    description\n    duration\n    latitude\n    longitude\n    potentialHelpers {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    helper {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    blockedUsers {\n      __typename\n      id\n      email\n      phoneNumber\n      firstName\n      lastName\n      preferredContact\n      description\n      requests {\n        __typename\n        nextToken\n      }\n      helperRatings\n      imageUrl\n      owner\n    }\n    owner\n  }\n}"

  public var owner: String

  public init(owner: String) {
    self.owner = owner
  }

  public var variables: GraphQLMap? {
    return ["owner": owner]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onDeleteRequest", arguments: ["owner": GraphQLVariable("owner")], type: .object(OnDeleteRequest.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onDeleteRequest: OnDeleteRequest? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onDeleteRequest": onDeleteRequest.flatMap { $0.snapshot }])
    }

    public var onDeleteRequest: OnDeleteRequest? {
      get {
        return (snapshot["onDeleteRequest"] as? Snapshot).flatMap { OnDeleteRequest(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onDeleteRequest")
      }
    }

    public struct OnDeleteRequest: GraphQLSelectionSet {
      public static let possibleTypes = ["Request"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("title", type: .nonNull(.scalar(String.self))),
        GraphQLField("requester", type: .nonNull(.object(Requester.selections))),
        GraphQLField("description", type: .nonNull(.scalar(String.self))),
        GraphQLField("duration", type: .nonNull(.scalar(String.self))),
        GraphQLField("latitude", type: .nonNull(.scalar(Double.self))),
        GraphQLField("longitude", type: .nonNull(.scalar(Double.self))),
        GraphQLField("potentialHelpers", type: .list(.object(PotentialHelper.selections))),
        GraphQLField("helper", type: .object(Helper.selections)),
        GraphQLField("blockedUsers", type: .list(.object(BlockedUser.selections))),
        GraphQLField("owner", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, title: String, requester: Requester, description: String, duration: String, latitude: Double, longitude: Double, potentialHelpers: [PotentialHelper?]? = nil, helper: Helper? = nil, blockedUsers: [BlockedUser?]? = nil, owner: String? = nil) {
        self.init(snapshot: ["__typename": "Request", "id": id, "title": title, "requester": requester.snapshot, "description": description, "duration": duration, "latitude": latitude, "longitude": longitude, "potentialHelpers": potentialHelpers.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "helper": helper.flatMap { $0.snapshot }, "blockedUsers": blockedUsers.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "owner": owner])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var title: String {
        get {
          return snapshot["title"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "title")
        }
      }

      public var requester: Requester {
        get {
          return Requester(snapshot: snapshot["requester"]! as! Snapshot)
        }
        set {
          snapshot.updateValue(newValue.snapshot, forKey: "requester")
        }
      }

      public var description: String {
        get {
          return snapshot["description"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "description")
        }
      }

      public var duration: String {
        get {
          return snapshot["duration"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "duration")
        }
      }

      public var latitude: Double {
        get {
          return snapshot["latitude"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "latitude")
        }
      }

      public var longitude: Double {
        get {
          return snapshot["longitude"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "longitude")
        }
      }

      public var potentialHelpers: [PotentialHelper?]? {
        get {
          return (snapshot["potentialHelpers"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { PotentialHelper(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "potentialHelpers")
        }
      }

      public var helper: Helper? {
        get {
          return (snapshot["helper"] as? Snapshot).flatMap { Helper(snapshot: $0) }
        }
        set {
          snapshot.updateValue(newValue?.snapshot, forKey: "helper")
        }
      }

      public var blockedUsers: [BlockedUser?]? {
        get {
          return (snapshot["blockedUsers"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { BlockedUser(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "blockedUsers")
        }
      }

      public var owner: String? {
        get {
          return snapshot["owner"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "owner")
        }
      }

      public struct Requester: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct PotentialHelper: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct Helper: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }

      public struct BlockedUser: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("phoneNumber", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("preferredContact", type: .list(.scalar(PreferredContact.self))),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("requests", type: .object(Request.selections)),
          GraphQLField("helperRatings", type: .list(.scalar(Double.self))),
          GraphQLField("imageUrl", type: .scalar(String.self)),
          GraphQLField("owner", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, email: String, phoneNumber: String, firstName: String? = nil, lastName: String? = nil, preferredContact: [PreferredContact?]? = nil, description: String? = nil, requests: Request? = nil, helperRatings: [Double?]? = nil, imageUrl: String? = nil, owner: String? = nil) {
          self.init(snapshot: ["__typename": "User", "id": id, "email": email, "phoneNumber": phoneNumber, "firstName": firstName, "lastName": lastName, "preferredContact": preferredContact, "description": description, "requests": requests.flatMap { $0.snapshot }, "helperRatings": helperRatings, "imageUrl": imageUrl, "owner": owner])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String {
          get {
            return snapshot["email"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "email")
          }
        }

        public var phoneNumber: String {
          get {
            return snapshot["phoneNumber"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var firstName: String? {
          get {
            return snapshot["firstName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return snapshot["lastName"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "lastName")
          }
        }

        public var preferredContact: [PreferredContact?]? {
          get {
            return snapshot["preferredContact"] as? [PreferredContact?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "preferredContact")
          }
        }

        public var description: String? {
          get {
            return snapshot["description"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "description")
          }
        }

        public var requests: Request? {
          get {
            return (snapshot["requests"] as? Snapshot).flatMap { Request(snapshot: $0) }
          }
          set {
            snapshot.updateValue(newValue?.snapshot, forKey: "requests")
          }
        }

        public var helperRatings: [Double?]? {
          get {
            return snapshot["helperRatings"] as? [Double?]
          }
          set {
            snapshot.updateValue(newValue, forKey: "helperRatings")
          }
        }

        public var imageUrl: String? {
          get {
            return snapshot["imageUrl"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "imageUrl")
          }
        }

        public var owner: String? {
          get {
            return snapshot["owner"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "owner")
          }
        }

        public struct Request: GraphQLSelectionSet {
          public static let possibleTypes = ["ModelRequestConnection"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("nextToken", type: .scalar(String.self)),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(nextToken: String? = nil) {
            self.init(snapshot: ["__typename": "ModelRequestConnection", "nextToken": nextToken])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nextToken: String? {
            get {
              return snapshot["nextToken"] as? String
            }
            set {
              snapshot.updateValue(newValue, forKey: "nextToken")
            }
          }
        }
      }
    }
  }
}